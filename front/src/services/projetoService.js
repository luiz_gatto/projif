import {api} from './api';

class ProjetoService{

    retrieveAllProjetos(){
        return api().get(`/projetos`)
    }

    saveProjetos(projeto){
        return api().post(`/projetos`, projeto)
    }
    
    deleteProjeto(idProjeto){
        return api().delete(`/projetos/${idProjeto}`)
    }

    updateProjetos(projeto){
        return api().put(`/projetos`, projeto)
    }
}

export default new ProjetoService();