export const ACCESS_TOKEN = "accessToken";

export const API_BASE_URL = window.location.hostname.includes("localhost")
  ? "http://localhost:8080"
  : "https://projif.ifsuldeminas.edu.br";
