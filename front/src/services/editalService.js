import {api} from './api';

class EditalService{

    retrieveAllEditais(){
        return api().get(`/publico/editais`)
    }

    saveEdital(edital){
        return api().post(`/publico/editais`, edital)
    }

    deleteEdital(idEdital){
        return api().delete(`/publico/editais/${idEdital}`)
    }

    updateEditais(edital){
        return api().put(`/publico/editais`, edital)
    }
}

export default new EditalService();