import {api} from './api';

class AnexoService{

    upload(foto){
        return api().post(`/file`, foto);
    };
  
    download(foto){
        return api().post(`/file/download`, foto, { responseType: "blob" });
    };

    salvar(fotos, id){
        return api().post(`/file/save/${id}`, fotos);
    };

}

export default new AnexoService();