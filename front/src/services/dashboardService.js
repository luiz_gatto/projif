import {api} from './api';
class DashboardService {
  dashboard() {
    return api().post(`/dashboard`);
  }
}

export default new DashboardService();
