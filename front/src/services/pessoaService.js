import {api} from './api';

class PessoaService{

    retrieveAllPessoas(){
        return api().get(`/publico/pessoas`)
    }

    savePessoas(pessoa){
        return api().post(`/publico/pessoas`, pessoa)
    }

    deletePessoas(idPessoa){
        return api().delete(`/publico/pessoas/${idPessoa}`)
    }

    updatePessoas(pessoa){
        return api().put(`/publico/pessoas`, pessoa)
    }
}

export default new PessoaService();