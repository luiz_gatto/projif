import {api} from './api';

class ComentarioService{

    retrieveAllComentarios(){
        return api().get(`/publico/comentarios`)
    }

    saveComentarios(comentario){
        return api().post(`/publico/comentarios`, comentario)
    }
    
    deleteComentario(idComentario){
        return api().delete(`/publico/comentarios/${idComentario}`)
    }

    updateComentario(comentario){
        return api().put(`/publico/comentarios`, comentario)
    }
}

export default new ComentarioService();