import {api} from './api';

class UsuarioService{

    retrieveAllUsuarios(){
        return api().get(`/publico/usuarios`)
    }

    saveUsuarios(usuario){
        return api().post(`/auth/signup`, usuario)
    }
    
    deleteUsuario(idUsuario){
        return api().delete(`/publico/usuarios/${idUsuario}`)
    }
}

export default new UsuarioService();