import {api} from './api';

class MembroService{

    retrieveAllMembros(){
        return api().get(`/publico/membros`)
    }

    saveMembro(membro){
        return api().post(`/publico/membros`, membro)
    }

    deleteMembro(idMembro){
        return api().delete(`/publico/membros/${idMembro}`)
    }

    updateMembros(membro){
        return api().put(`/publico/membros`, membro)
    }
}

export default new MembroService();