import {api} from './api';

class AreaConhecimentoService{

    retrieveAllAreas(){
        return api().get(`/publico/areas`)
    }

    saveAreas(areaConhecimento){
        return api().post(`/publico/areas`, areaConhecimento)
    }

    deleteArea(idArea){
        return api().delete(`/publico/areas/${idArea}`)
    }

    updateAreas(area){
        return api().put(`/publico/areas`, area)
    }
}

export default new AreaConhecimentoService();