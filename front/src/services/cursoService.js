import {api} from './api';
class CursoService{

    retrieveAllCursos(){
        return api().get(`/publico/cursos`)
    }

    saveCursos(curso){
        return api().post(`/publico/cursos`, curso)
    }

    deleteCursos(idCurso){
        return api().delete(`/publico/cursos/${idCurso}`)
    }

    updateCursos(curso){
        return api().put(`/publico/cursos`, curso)
    }
}

export default new CursoService();