import { api } from "./api";
class LoginService {
  login(usuario) {
    return api().post(`/auth/login`, usuario);
  }
  recuperarSenha(email) {
    return api().post(
      `/auth/recuperarEmail/aadgdfgdfvtcwtetvdrfgdfysgaet/${email}`,
      {}
    );
  }

  validarHash(hash) {
    return api().post(
      `/auth/validarHash/${hash}--4565456445-SFGDGDFG-5DDFFAFW-DFGDGDFGDFG-DSFFDF`,
      {}
    );
  }

  redefinirSenha(redefinicao, hash) {
    return api().post(
      `/auth/redefinirSenha/${hash}--4565456445-SFGDGDFG-5DDFFAFW-DFGDGDFGDFG-DSFFDF`,
      redefinicao
    );
  }
}

export default new LoginService();
