import Modal from 'react-bootstrap/Modal';
import styles from '../ModalEdital/styles.module.scss';
import EditalService from '../../services/editalService';

import { Form, Input, message } from 'antd';
import { Row, Col } from 'react-bootstrap';

export function ModalEdital(props) {

  const [form] = Form.useForm();

  const onFinish = (values) => {
    EditalService.saveEdital(values);
    form.resetFields();
    message.success("Edital salvo com sucesso!")
  }


  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      dialogClassName={styles.ModalEditais}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title className={styles.title}>
          Cadastrar novo edital
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish}>

          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Número do edital:"
                  name="numero"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o número do edital!',
                    },
                  ]}>
                  <Input placeholder="Insira o número do edital" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Ano do edital:"
                  name="ano"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o ano do edital!',
                    },
                  ]}>
                  <Input placeholder="Insira o ano do edital" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Orgão:"
                  name="orgao"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o orgão do edital!',
                    },
                  ]}>
                  <Input placeholder="Insira o orgão do edital" />
                </Form.Item>
              </Col>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Descrição:"
                  name="descricao"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a descrição!',
                    },
                  ]}>
                  <Input placeholder="Insira a descrição" />
                </Form.Item>
              </Col>

              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit" onClick={props.onHide} >
                  Salvar
                </button>
              </Col>
            </Row>
          </div>
        </Form>
      </Modal.Body>

    </Modal>
  );
}