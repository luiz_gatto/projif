import styles from '../ListItemCursos/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
  return (
    <div className={styles.listagem}>
      <Row>
        <Col md={2}>
          <img src="/curso.png" alt="icon" />
        </Col>
        <Col md={10}>
          <h1>{props.nome} </h1>
          <h2><b>Modalidade:</b> {props.modalidade}</h2>
          <h2><b>Campus:</b> {props.campus}</h2>
        </Col>
      </Row>
    </div>
  );
}

export default ListItem;