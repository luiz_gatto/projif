import styles from '../ListItemComentarios/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
    return (
        <div className={styles.listagem}>
            <h1 className={styles.titulo}>{props.titulo}</h1>

            <div className={styles.comentario}>
                <p>{props.conteudo}</p>
            </div>
            
            <Row className={styles.id}>
                <Col md={2}>
                    <img src="/pessoa.png" alt="icon" />
                </Col>

                <Col md={10}>
                    <h2> {props.nome} <br /> {props.email}</h2>
                </Col>
            </Row>
        </div>
    );
}

export default ListItem;
