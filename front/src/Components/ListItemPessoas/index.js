import styles from '../ListItemPessoas/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
  return (
    <div className={styles.listagem}>
      <Row>

        <Col md={11}>
          <Row>
            <Col md={1}>
              <img src="/pessoa.png" alt="icon" />
            </Col>
            <Col md={11}>
              <h1><b> {props.nome}</b></h1>
            </Col>
          </Row>
          <h2><b>Email:</b> {props.email}</h2>
          <h2><b>Lattes:</b> {props.lattes}</h2>
        </Col>
      </Row>
    </div>
  );
}

export default ListItem;