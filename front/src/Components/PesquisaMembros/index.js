import React, { useState } from 'react';
import MembroService from "../../services/membroService";
import styles from '../PesquisaMembros/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';

function PesquisaMembro(props) {

    const [search, setSearch] = useState('');

    const [membros, setMembros] = useState([]);

    useEffect(() => {
        refreshMembros();
        return () => {
        }
    }, [])

    async function refreshMembros() {
        MembroService.retrieveAllMembros()
            .then(
                response => {
                    setMembros(response)
                }
            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = membros.filter(membro => membro.pessoa.nome.toLowerCase().indexOf(search) !== -1
            || membro.projeto.titulo.toLowerCase().indexOf(search) !== -1);

        props.setMembros(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por pessoa ou projeto"
                                aria-label="Pesquise por pessoa ou projeto"
                                onChange={handleSearchChange}

                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>

                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaMembro;