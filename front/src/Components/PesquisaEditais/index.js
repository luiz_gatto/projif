import React, { useState } from 'react';

import EditalService from "../../services/editalService";
import styles from '../PesquisaEditais/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';

function PesquisaEdital(props) {

    const [search, setSearch] = useState('');

    const [editais, setEditais] = useState([]);

    useEffect(() => {
        refreshEditais();
        return () => {
        }
    }, [])

    async function refreshEditais() {
        EditalService.retrieveAllEditais()
            .then(
                response => {
                    setEditais(response)
                }
            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = editais.filter(edital => edital.descricao.toLowerCase().indexOf(search) !== -1
            || edital.orgao.toLowerCase().indexOf(search) !== -1
            || edital.numero.toString().indexOf(search) !== -1
            || edital.ano.toString().indexOf(search) !== -1);
        props.setEditais(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por número, ano, descrição ou orgão"
                                aria-label="Pesquise por número, ano, descrição ou orgão"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaEdital;