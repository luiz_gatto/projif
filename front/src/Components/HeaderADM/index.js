import styles from "../HeaderADM/styles.module.scss";
import { Container } from "react-bootstrap";
import { Dropdown, SplitButton } from "react-bootstrap";
import "../../App.css";
import { ACCESS_TOKEN } from "../../services/constantes";

export function Header() {
  const logout = () => {
    localStorage.removeItem(ACCESS_TOKEN);
    // Alert.success("Logout efetuado com sucesso!");
  };

  return (
    <div className={styles.headerContainer}>
      <header>
        <a href="/#/homeADM">
          <img src="/logo-branca.png" alt="Icon Projif" />
        </a>
      </header>

      <div>
        {["end"].map((direction) => (
          <SplitButton key={direction} drop={direction} title={`Cursos`}>
            <div className={styles.item}>
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="1"
                href="/#/cursos"
              >
                Cadastrar
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="2"
                href="/#/listagem_cursosADM"
              >
                Ver Listagem
              </Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>

      <div>
        {["end"].map((direction) => (
          <SplitButton key={direction} drop={direction} title={`Editais`}>
            <div className={styles.item}>
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="1"
                href="/#/editais"
              >
                Cadastrar
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="2"
                href="/#/listagem_editaisADM"
              >
                Ver Listagem
              </Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>
      <div>
        {["end"].map((direction) => (
          <SplitButton key={direction} drop={direction} title={`Membros`}>
            <div className={styles.item}>
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="1"
                href="/#/membros"
              >
                Cadastrar
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="2"
                href="/#/listagem_membrosADM"
              >
                Ver Listagem
              </Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>

      <div>
        {["end"].map((direction) => (
          <SplitButton key={direction} drop={direction} title={`Pessoas`}>
            <div className={styles.item}>
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="1"
                href="/#/pessoas"
              >
                Cadastrar
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="2"
                href="/#/listagem_pessoasADM"
              >
                Ver Listagem
              </Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>

      <div>
        {["end"].map((direction) => (
          <SplitButton key={direction} drop={direction} title={`Projetos`}>
            <div className={styles.item}>
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="1"
                href="/#/projetos"
              >
                Cadastrar
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="2"
                href="/#/listagem_projetosADM"
              >
                Ver Listagem
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="3"
                href="/#/listagem_areasADM"
              >
                Áreas do Conhecimento{" "}
              </Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>

      <div>
        {["end"].map((direction) => (
          <SplitButton key={direction} drop={direction} title={`Usuários`}>
            <div className={styles.item}>
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="1"
                href="/#/usuarios"
              >
                Cadastrar
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="2"
                href="/#/listagem_usuarios"
              >
                Ver Listagem
              </Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>

      <div>
        {["end"].map((direction) => (
          <SplitButton key={direction} drop={direction} title={`Dashboard`}>
            <div className={styles.item}>
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="1"
                href="/#/dashboard"
              >
                Visualizar Dashboard
              </Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>
      <div>
        {["end"].map((direction) => (
          <SplitButton key={direction} drop={direction} title={`Feed`}>
            <div className={styles.item}>
              <Dropdown.Item
                className={styles.itemMenu}
                eventKey="1"
                href="/#/listagem_comentariosADM"
              >
                Visualizar Todos os Comentários
              </Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>

      <ul>
        <li className={styles.sair}>
          {" "}
          <a href="/#/login" onClick={logout}>
            <img src="/sair.png" alt="icon Sair" />
          </a>
        </li>
      </ul>

      <footer>
        <Container>
          <img src="/logo-IF.png" alt="IF" />
        </Container>
      </footer>
    </div>
  );
}
