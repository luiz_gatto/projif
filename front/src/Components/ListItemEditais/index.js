import styles from '../ListItemEditais/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
  return (
    <div className={styles.listagem}>
      <Row>
        <Col md={12}>
          <Row>
            <Col md={2}>
              <img src="/visualizar.png" alt="icon" />
            </Col>
            <Col md={10}>
              <h1 class="text-center"><b>Edital: </b> {props.numero} / {props.ano}</h1>
            </Col>
          </Row>

          <h2><b>Descrição:</b> {props.descricao}</h2>
          <h2><b>Orgão:</b> {props.orgao}</h2>
        </Col>
      </Row>
    </div>
  );
}

export default ListItem;