import styles from '../ListItemComentariosADM/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItemADM(props) {
    return (
        <div className={styles.listagem}>
            <div className={styles.status}>
                <p>{props.status}</p>
            </div>
            <h1 className={styles.titulo}>{props.titulo}</h1>

            <div className={styles.comentario}>
                <p>{props.conteudo}</p>
            </div>

            <Row className={styles.id}>
                <Col md={2}>
                    <img src="/pessoa.png" alt="icon" />
                </Col>

                <Col md={10}>
                    <h2> {props.nome} <br /> {props.email}</h2>
                </Col>
            </Row>
        </div>
    );
}

export default ListItemADM;
