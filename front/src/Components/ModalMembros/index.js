import Modal from 'react-bootstrap/Modal';
import Button from "react-bootstrap/Button";
import styles from '../ModalMembros/styles.module.scss';

import { Link } from 'react-router-dom';

export function ModalMembros(props) {

  function fechar(){
    props.onHide();
    window.location.reload();
  }
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      dialogClassName={styles.ModalMembros}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title className={styles.title}>
          Membros do projeto
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          Deseja inserir os membros do projeto agora?
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={fechar} className={styles.btnNao}>
          Não</Button>
        <Link to="/membros">
          <Button className={styles.btnSim}>
            Sim
          </Button>
        </Link>
      </Modal.Footer>
    </Modal>
  );
}