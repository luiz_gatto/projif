import styles from '../ListItemAreas/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
  return (
    <div className={styles.listagem}>
      <Row>
        <Col md={2}>
          <img src="/area.png" alt="icon" />
        </Col>
        <Col md={10}>
          <h1>{props.codArea} </h1>
          <h2><b>{props.area}</b></h2>
        </Col>
      </Row>
    </div>
  );
}

export default ListItem;