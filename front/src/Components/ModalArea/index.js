import "antd/dist/antd.css"
import Modal from 'react-bootstrap/Modal';
import InputMask from "react-input-mask";
import styles from '../ModalCursos/styles.module.scss';
import AreaService from '../../services/areaConhecimentoService';

import { Form, Input, message } from 'antd';
import { Row, Col } from 'react-bootstrap';

const InputCod = (props) => (
  <InputMask mask="9.99.99.99-9"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o código" />
);

export function ModalArea(props) {

  const [form] = Form.useForm();

  const onFinish = (values) => {
    
    form.resetFields();

    AreaService.saveAreas(values).then(() => {
      message.success("Área incluído com sucesso!")
    })
      .catch((error) => {
        message.error("Cadastro não foi concluído!")
      });
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      dialogClassName={styles.ModalCursos}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title className={styles.title}>
          Cadastrar nova área do conhecimento
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Código da Área:"
                  name="codArea"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o código da área',
                    },
                  ]}>
                  <InputCod placeholder="Insira o código da área" />

                </Form.Item>
              </Col>
              <Col md={8} className={styles.field}>
                <Form.Item
                  label="Área do Conhecimento:"
                  name="area"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a área',
                    },
                  ]}>
                  <Input placeholder="Insira a área" />
                </Form.Item>
              </Col>

              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit"  onClick={props.onHide} >
                  Salvar
                </button>
              </Col>

            </Row>
          </div>
        </Form>
      </Modal.Body>

    </Modal>
  );
}