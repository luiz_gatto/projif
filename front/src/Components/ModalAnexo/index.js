import Modal from 'react-bootstrap/Modal';
import styles from '../ModalAnexo/styles.module.scss';
import ImageUploader from "react-images-upload";
import React, {  useState } from "react";
import AnexoService from '../../services/anexoService';
import { saveAs } from "file-saver";

export function ModalAnexo(props) {
    const [anexos, setAnexos] = useState([]);

    const uploadFotoMethod = (pictureFiles) => {
        const formData = new FormData();
        formData.append("arquivo", pictureFiles[pictureFiles.length - 1]);
        // setLoading(true);
        AnexoService.upload(formData)
          .then((data) => {
            // setLoading(false);
            setAnexos([
              ...anexos,
              { nome: pictureFiles[pictureFiles.length - 1].name, key: data },
            ]);
            pictureFiles = [];
          })
        //   .catch(() => setLoading(false));
      };

    const salvarArquivo = (anexos) => {
      AnexoService.salvar(anexos, props.idProjeto);
      props.etapaMembro();
    }

    const excluirArquivo = (anexos) => {
      AnexoService.deletar(anexos, props.idProjeto);
      props.etapaMembro();
    }
    
      const downLoadAnexo = (anexo) => {
        // setLoading(true);
        AnexoService.download(anexo).then((arquivo) => {
          saveAs(arquivo, anexo.nome);
        //   setLoading(false);
        });
      };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      dialogClassName={styles.ModalMembros}
      centered
    >
      <Modal.Header>
        <Modal.Title className={styles.title}>
         Anexos do Projeto
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
         <div className="col-12">
            
            <ImageUploader
              className={styles.upload}
              withIcon={true}
              onChange={(pictureFiles) => uploadFotoMethod(pictureFiles)}
              imgExtension={[
                ".pdf"
              ]}
              accept="application/pdf"
              maxFileSize={5242880}
              singleImage={true}
              buttonText="Enviar arquivo"
              label="Tamanho máximo: 5mb, formatos: pdf"
              fileTypeError=" não é suportado"
              fileSizeError="Imagem muito grande, máximo 5MB"
              
            />
          </div>
          {anexos.length ? (
            <div>
              <h5>Anexos:</h5>
            </div>
          ) : null}
          {anexos.map((anexo) => {
            return (
              <section className="col-6">
                <div className="box-shadow text-justify">
                  <h6  className="col-6 text-muted">{anexo.nome}</h6>
                  <button
                    type="button"
                    class="col-md-2"
                    onClick={() => {
                      downLoadAnexo(anexo);
                    }}
                   
                  >
                    <img src="/download.png" alt="icon" />
                  </button>
                  <button
                    type="button"
                    class="col-md-2"
                    onClick={() => {
                      
                    }}
                   
                  >
                    <img src="/excluir.png" alt="icon" />
                  </button>
                </div>
              </section>
            );
          })}
           <div className="col-12 text-center" >
                <button className={styles.buttonSubmit} onClick={()=>salvarArquivo(anexos)} >
                  Salvar
                </button>
              </div>
         
      </Modal.Body>
    </Modal>
  );
}