import styles from '../ListItemMembros/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
  return (
    <div className={styles.listagem}>
      <Row>
        <Col md={2}>
          <img src="/pessoa.png" alt="icon" />
        </Col>
        <Col md={10}>


          <h1><b>{props.nome}</b></h1>


          <h2><b>Tipo:</b> {props.tipo}</h2>
          <h2><b>Modalide:</b> {props.modalidade}</h2>
          <h2><b>Projeto:</b> {props.projeto}</h2>

        </Col>
      </Row>
    </div>
  );
}

export default ListItem;