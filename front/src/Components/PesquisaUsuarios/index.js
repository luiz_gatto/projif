import React, { useState } from 'react';
import UsuarioService from "../../services/usuarioService";
import styles from '../PesquisaPessoas/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';


function PesquisaUsuarios(props) {

    const [search, setSearch] = useState('');

    const [usuarios, setUsuarios] = useState([]);



    useEffect(() => {
        refreshUsuarios();
        return () => {
        }
    }, [])

    async function refreshUsuarios() {
        UsuarioService.retrieveAllUsuarios()
            .then(
                response => {
                    setUsuarios(response)
                }
            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = usuarios.filter(usuario => usuario.nome.toLowerCase().indexOf(search) !== -1 || usuario.cpf.toLowerCase().indexOf(search) !== -1);
        props.setUsuario(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por nome e CPF"
                                aria-label="Pesquise por nome e CPF"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaUsuarios;