import React, { useState } from 'react';
import comentarioService from "../../services/comentarioService";
import styles from '../PesquisaComentarios/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';


function PesquisaComentarios(props) {

    const [search, setSearch] = useState('');

    const [comentarios, setComentarios] = useState([]);

    useEffect(() => {
        refreshComentarios();
        return () => {
        }
    }, [])

    async function refreshComentarios() {
        comentarioService.retrieveAllComentarios()
            .then(
                response => {
                    setComentarios(response)
                }

            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = comentarios.filter(comentario => comentario.projeto.titulo.toLowerCase().indexOf(search) !== -1);
        props.setComentarios(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por projeto"
                                aria-label="Pesquise por projeto"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaComentarios;