import React, { useState } from 'react';
import CursoService from "../../services/cursoService";
import styles from '../PesquisaCursos/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';

function PesquisaCurso(props) {

    const [search, setSearch] = useState('');

    const [cursos, setCursos] = useState([]);

    useEffect(() => {
        refreshCursos();
        return () => {
        }
    }, [])

    async function refreshCursos() {
        CursoService.retrieveAllCursos()
            .then(
                response => {
                    setCursos(response)
                }
            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = cursos.filter(curso => curso.nome.toLowerCase().indexOf(search) !== -1
            || curso.campus.toLowerCase().indexOf(search) !== -1);
        props.setCursos(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por nome ou campus"
                                aria-label="Pesquise por nome ou campus"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaCurso;