import React, { useState } from 'react';

import PessoaService from "../../services/pessoaService";
import styles from '../PesquisaPessoas/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';

function PesquisaPessoa(props) {

    const [search, setSearch] = useState('');
    const [pessoas, setPessoas] = useState([]);

    useEffect(() => {
        refreshPessoas();
        return () => {
        }
    }, [])

    async function refreshPessoas() {
        PessoaService.retrieveAllPessoas()
            .then(
                response => {
                    setPessoas(response)
                }
            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = pessoas.filter(pessoa => pessoa.nome.toLowerCase().indexOf(search) !== -1 || pessoa.cpf.toLowerCase().indexOf(search) !== -1);
        props.setPessoas(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por nome ou CPF"
                                aria-label="Pesquise por nome ou CPF"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaPessoa;