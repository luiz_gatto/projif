import Modal from 'react-bootstrap/Modal';
import styles from '../ModalCursos/styles.module.scss';
import CursoService from '../../services/cursoService';

import { Form, Input, message } from 'antd';
import { Row, Col } from 'react-bootstrap';

export function ModalCursos(props) {

  const [form] = Form.useForm();

  const onFinish = (values) => {
    CursoService.saveCursos(values);
    form.resetFields();
    message.success("Curso salvo com sucesso!")
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      dialogClassName={styles.ModalCursos}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title className={styles.title}>
          Cadastrar novo curso
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>
              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Curso:"
                  name="nome"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o nome do curso',
                    },
                  ]}>
                  <Input placeholder="Insira o nome do curso" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Campus:"
                  name="campus"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o campus',
                    },
                  ]}>
                  <Input placeholder="Insira o campus" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Modalidade:"
                  name="modalidade"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a modalidade do curso',
                    },
                  ]}>
                  <select >
                    <option value="" disabled selected>Selecione a modalidade</option>
                    <option value="SUPERIOR">Superior</option>
                    <option value="TÉCNICO">Técnico</option>
                  </select>
                </Form.Item>
              </Col>
              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit" >
                  Salvar
                </button>
              </Col>
            </Row>
          </div>
        </Form>
      </Modal.Body>

    </Modal>
  );
}