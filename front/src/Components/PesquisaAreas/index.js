import React, { useState } from 'react';
import AreaConhecimentoService from "../../services/areaConhecimentoService";
import styles from '../PesquisaPessoas/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';

function PesquisaArea(props) {

    const [search, setSearch] = useState('');
    const [areas, setAreas] = useState([]);

    useEffect(() => {
        refreshAreas();
        return () => {
        }
    }, [])

    async function refreshAreas() {
        AreaConhecimentoService.retrieveAllAreas()
            .then(
                response => {
                    setAreas(response)
                }
            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = areas.filter(area => area.codArea.toLowerCase().indexOf(search) !== -1 || area.area.toLowerCase().indexOf(search) !== -1);
        props.setAreas(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise código ou área"
                                aria-label="Pesquise código ou área"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaArea;