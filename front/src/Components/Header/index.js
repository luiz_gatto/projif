import styles from "../Header/styles.module.scss";
import { Container } from "react-bootstrap";

export function HeaderVisitante() {
  return (
    <div className={styles.headerContainer}>
      <header>
        <a href="/#/home">
          <img src="/logo-branca.png" alt="icon" />
        </a>
      </header>

      <a class="btn visitante" href="/#/listagem_projetos">
        Projetos
      </a>
      <a class="btn visitante" href="/#/listagem_pessoas">
        Pessoas
      </a>
      <a class="btn visitante" href="/#/listagem_membros">
        Membros
      </a>
      <a class="btn visitante" href="/#/listagem_comentarios">
        Feed
      </a>
      <a class="btn visitante" href="/#/listagem_editais">
        Editais
      </a>
      <a class="btn visitante" href="/#/listagem_cursos">
        Cursos
      </a>
      <a class="btn visitante" href="/#/listagem_areas">
        Áreas do <br />
        Conhecimento
      </a>

      <li className={styles.login}>
        {" "}
        <a href="/#/login">
          <img src="/Login_branco.png " alt="Icon Projif" />
        </a>
      </li>

      <footer>
        <Container>
          <img src="/logo-IF.png" alt="IF" />
        </Container>
      </footer>
    </div>
  );
}
