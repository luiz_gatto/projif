import styles from '../ListItemProjetos/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
    return (
        <div className={styles.listagem}>
            <Row>
                <Col md={1}>
                    <img src="/visualizar.png" alt="icon" />
                </Col>
                <Col md={11}>
                    <h1 class="text-left"><b>{props.titulo}</b></h1>
                    <h2><br /><b>Edital: </b> {props.edital}</h2>
                    <h2><b>Tipo: </b> {props.tipo}</h2>
                    <h2><b>Área do Conhecimento: </b>{props.area}</h2>
                </Col>
            </Row>

        </div>
    );
}

export default ListItem;