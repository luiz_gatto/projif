import Modal from 'react-bootstrap/Modal';
import InputMask from "react-input-mask";
import styles from '../ModalEdital/styles.module.scss';
import PessoaService from '../../services/pessoaService';

import { Form, Input, message } from 'antd';
import { Row, Col } from 'react-bootstrap';


const InputTel = (props) => (
  <InputMask mask="(99)99999-9999"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o número de telefone" />
);

const InputCEP = (props) => (
  <InputMask mask="99999-999"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o CEP" />
);

const InputCPF = (props) => (
  <InputMask mask="999.999.999-99"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o CPF" />
);


export function ModalPessoas(props) {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    PessoaService.savePessoas(values);
    form.resetFields();
    message.success("Pessoa salva com sucesso!")
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      dialogClassName={styles.ModalEditais}
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title className={styles.title}>
          Cadastrar nova pessoa
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish}>

          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Nome:"
                  name="nome"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o nome completo',
                    },
                  ]}>
                  <Input placeholder="Insira o nome completo" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="CPF:"
                  name="cpf"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o CPF',
                    },
                  ]}>
                  <InputCPF />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Data de Nascimento: "
                  name="data_nascimento"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a data de nascimento',
                    },
                  ]}>
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Currículo Lattes:"
                  name="lattes"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o link do currículo lattes',
                    },
                  ]}>
                  <Input placeholder="Insira o link do currículo lattes" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Email:"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o endereço de email',
                    },
                  ]}>
                  <Input type="email" placeholder="Insira o endereço de email" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Telefone:"
                  name="telefone"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o número de telefone',
                    },
                  ]}>
                  <InputTel />
                </Form.Item>
              </Col>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Logradouro:"
                  name="logradouro"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o endereço!',
                    },

                  ]}>
                  <Input placeholder="Insira o endereço" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Nº:"
                  name="numero"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o número!',
                    },

                  ]}>
                  <Input placeholder="Insira o número" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Bairro:"
                  name="bairro"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o bairro!',
                    },

                  ]}>
                  <Input placeholder="Insira o bairro" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Cidade:"
                  name="cidade"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a cidade!',
                    },

                  ]}>
                  <Input placeholder="Insira a cidade" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item
                  label="CEP:"
                  name="cep"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o CEP!',
                    },

                  ]}>
                  <InputCEP/>
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Estado:"
                  name="estado"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o Estado!',
                    },

                  ]}>
                  <Input placeholder="Insira o Estado" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item
                  label="UF:"
                  name="uf"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a UF!',
                    },

                  ]}>
                  <Input maxLength="2" placeholder="Insira a UF"/>
                </Form.Item>
              </Col>

              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit" >
                  Salvar
                </button>
              </Col>
            </Row>
          </div>
        </Form>
      </Modal.Body>

    </Modal>
  );
}