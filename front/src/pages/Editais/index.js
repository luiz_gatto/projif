import styles from '../Editais/styles.module.scss';
import EditalService from '../../services/editalService';

import { Form, Input, message } from 'antd';
import { Row, Col } from 'react-bootstrap';
import { Header } from '../../Components/HeaderADM';

export function Editais(props) {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    
    form.resetFields();

    EditalService.saveEdital(values).then(() => {
      message.success("Edital incluído com sucesso!")
    })
      .catch((error) => {
        message.error("Cadastro não foi concluído!")
      });
  }

  return (
    <div className={styles.wrapper}>

      <Header />

      <main className={styles.App}>
        <h1>Cadastrar Editais</h1>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish}>

          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Número do edital:"
                  name="numero"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o número do edital!',
                    },
                  ]}>
                  <Input placeholder="Insira o número do edital" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Ano do edital:"
                  name="ano"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o ano do edital!',
                    },
                  ]}>
                  <Input placeholder="Insira o ano do edital" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Orgão:"
                  name="orgao"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o orgão do edital!',
                    },
                  ]}>
                  <Input placeholder="Insira o orgão do edital" />
                </Form.Item>
              </Col>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Descrição:"
                  name="descricao"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a descrição!',
                    },
                  ]}>
                  <Input placeholder="Insira a descrição" />
                </Form.Item>
              </Col>


              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit">
                  Salvar
                </button>
              </Col>
            </Row>
          </div>
        </Form>
      </main>
    </div>

  );
}