import styles from '../Cursos/styles.module.scss';
import CursoService from '../../services/cursoService';

import { Form, Input, message } from 'antd';
import { Row, Col } from 'react-bootstrap';
import { Header } from '../../Components/HeaderADM';

export function Cursos(props) {

  const [form] = Form.useForm();

  const onFinish = (values) => {
    form.resetFields();

    CursoService.saveCursos(values).then(() => {
      message.success("Curso incluído com sucesso!")
    })
      .catch((error) => {
        message.error("Cadastro não foi concluído!")
      });
  }

  return (
    <div className={styles.wrapper}>

      <Header />

      <main className={styles.App}>
        <h1>Cadastrar Cursos</h1>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>
              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Curso:"
                  name="nome"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o nome do curso',
                    },
                  ]}>
                  <Input placeholder="Insira o nome do curso" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Campus:"
                  name="campus"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o campus',
                    },
                  ]}>
                  <Input placeholder="Insira o campus" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Modalidade:"
                  name="modalidade"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a modalidade do curso',
                    },
                  ]}>
                  <select >
                    <option value="" disabled selected>Selecione a modalidade</option>
                    <option value="SUPERIOR">Superior</option>
                    <option value="TÉCNICO">Técnico</option>
                  </select>
                </Form.Item>
              </Col>
              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit" >
                  Salvar
                </button>
              </Col>
            </Row>
          </div>
        </Form>
      </main>
    </div>

  );
}