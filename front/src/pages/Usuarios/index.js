import styles from '../Usuarios/styles.module.scss'
import React from 'react';
import InputMask from "react-input-mask";
import UsuarioService from "../../services/usuarioService";

import { Row, Col } from 'react-bootstrap';
import { Form, message, Input } from 'antd';
import { Header } from '../../Components/HeaderADM';


const InputCPF = (props) => (
  <InputMask mask="999.999.999-99"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o CPF" />
);

export default function CadastrarUsuarios() {

  const [form] = Form.useForm();

  const onFinish = (values) => {
 
    form.resetFields();   

    UsuarioService.saveUsuarios(values).then(() => {
      message.success("Usuário incluído com sucesso!")
    })
      .catch((error) => {
        message.error("Cadastro não foi concluído!")
      });
  }

  const onFinishFailed = (erroInfo) => {

    console.log('Failed:', erroInfo)
  }

  return (
    <div className={styles.wrapper}>

      <Header />

      <main className={styles.App}>
        <h1>Cadastrar Usuários</h1>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish} onFinishFailed={onFinishFailed}>

          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Nome:"
                  name="nome"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o nome do usuário!',
                    },

                  ]}>

                  <Input placeholder="Insira o nome do usuário" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="CPF:"
                  name="cpf"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o cpf do usuário!',
                    },

                  ]}>
                  <InputCPF />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Email:"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o email do usuário!',
                    },

                  ]}>

                  <Input type="email" placeholder="Insira o email do usuário" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Senha:"
                  name="senha"
                  rules={[
                    {
                      required: true,
                      message: 'Insira senha do usuário!',
                    },

                  ]}>

                  <Input placeholder="Insira a senha do usuário" type="password" />
                </Form.Item>
              </Col>

              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit">
                  Salvar
                </button>
              </Col>
            </Row>

          </div>
        </Form>
      </main>
    </div>
  );
}
