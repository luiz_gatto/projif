import "antd/dist/antd.css"
import styles from '../Projetos/styles.module.scss'
import React from 'react';

import { Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react'
import { Form, message, Input, Select } from 'antd';


import { Header } from '../../Components/HeaderADM';
import { ModalMembros } from '../../Components/ModalMembros';
import { ModalEdital } from '../../Components/ModalEdital';
import { ModalArea } from "../../Components/ModalArea";
import { ModalAnexo } from "../../Components/ModalAnexo";

import ProjetoService from "../../services/projetoService";
import AreaConhecimentoService from "../../services/areaConhecimentoService";
import EditalService from "../../services/editalService";


const { Option } = Select;

export default function CadastrarProjetos() {

  const [areas, setAreas] = useState([]);
  const [areaConhecimento, setArea] = useState([]);

  const [editais, setEditais] = useState([]);
  const [edital, setEdital] = useState([]);
  const [idProjeto, setIdProjero] = useState(null);

  const [form] = Form.useForm();


  useEffect(() => {
    refresh();
  }, [])

  async function refresh() {
    AreaConhecimentoService.retrieveAllAreas()
      .then(
        response => {
          setAreas(response)
        }
      )
    EditalService.retrieveAllEditais()
      .then(
        response => {
          setEditais(response)
        }
      )
  }

  const onFinish = async (values) => {
    values.edital = {
      idEdital: edital
    };

    values.areaConhecimento = {
      idArea: areaConhecimento
    };


    const {idProjeto} =  await ProjetoService.saveProjetos(values);

    setIdProjero(idProjeto);

    form.resetFields();
    message.success("Projeto salvo com sucesso!")

    

  }

  const onFinishFailed = (erroInfo) => {

    console.log('Failed:', erroInfo)
  }

  function handleChangeArea(value) {
    setArea(value);
  }

  function handleChangeEdital(value) {
    setEdital(value);
  }

  const etapaMembro = () => {
    setModalShowAnexo(false);
    setModalShowMembros(true);
  }

  const [modalShowAnexo, setModalShowAnexo] = React.useState(false);
  const [modalShowMembros, setModalShowMembros] = React.useState(false);
  const [modalShowEdital, setModalShowEdital] = React.useState(false);
  const [modalShowArea, setModalShowArea] = React.useState(false);

  return (
    <div className={styles.wrapper}>

      <Header />

      <main className={styles.App}>
        <h1>Cadastrar Projetos</h1>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish} onFinishFailed={onFinishFailed}>

          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>
              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Título do projeto:"
                  name="titulo"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o titulo do projeto!',
                    },

                  ]}>

                  <Input placeholder="Insira o título" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Edital:"
                  name="edital.idEdital"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o edital',
                    },
                  ]}>


                  <Select onChange={handleChangeEdital} >
                    <Option value="" disabled selected>Selecione o edital</Option>
                    {editais.map((item) => {
                      return (
                        <Option key={item.idEdital} value={item.idEdital} >
                          {item.numero + "/" + item.ano + " - " + item.descricao}
                        </Option>
                      )
                    })}
                  </Select>
                </Form.Item>
              </Col>

              <Col md={1}>
                <button className={styles.buttonNovo} type="button" onClick={() => setModalShowEdital(true)}>
                  Novo
                </button>
              </Col>
              <Col md={1}>
                <button className={styles.buttonRecarregar} type="button" onClick={refresh}>
                  <img src="/recarregar.png" alt="logo" />
                </button>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Acesso:"
                  name="acesso"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o tipo de acesso',
                    },
                  ]}>
                  <select >
                    <option value="" disabled selected>Selecione o acesso</option>
                    <option value="PÚBLICO">Público</option>
                    <option value="SIGILOSO">Sigiloso</option>
                  </select>
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Área do conhecimento:"
                  name="areaConhecimento.idArea"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a área do conhecimento',
                    },
                  ]}>

                  <Select onChange={handleChangeArea} >
                    <Option value="" disabled selected>Selecione a área do conhecimento</Option>
                    {areas.map((item) => {
                      return (
                        <Option key={item.idArea} value={item.idArea} >
                          {item.codArea + " - " + item.area}
                        </Option>
                      )
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col md={1}>
                <button className={styles.buttonNovo} type="button" onClick={() => setModalShowArea(true)}>
                  Novo
                </button>
              </Col>
              <Col md={1}>
                <button className={styles.buttonRecarregar} type="button" onClick={refresh}>
                  <img src="/recarregar.png" alt="logo" />
                </button>
              </Col>
              
              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Tipo:"
                  name="tipo"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o tipo do projeto',
                    },
                  ]}>
                  <select>
                    <option value="" disabled selected>Selecione o tipo</option>
                    <option value="ENSINO">Ensino</option>
                    <option value="EXTENSÃO">Extensão</option>
                    <option value="PESQUISA">Pesquisa</option>
                    <option value="TCC">TCC</option>
                  </select>
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Início do projeto:"
                  name="data_inicio"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a data de início',
                    },
                  ]}>
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Finalização do projeto:"
                  name="data_fim"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a data de finalização',
                    },
                  ]}>
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Status:"
                  name="status"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o status',
                    },
                  ]}>
                  <select>
                    <option value="" disabled selected>Selecione o status</option>
                    <option value="ANDAMENTO">Andamento</option>
                    <option value="FINALIZADO">Finalizado</option>
                  </select>
                </Form.Item>
              </Col>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Resumo:"
                  name="resumo"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o resumo',
                    },
                  ]}>
                  <textarea
                    rows="3" 
                    placeholder="Insira o resumo do projeto"
                    name="resumo"
                  />
                </Form.Item>
              </Col>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Palavras-chave:"
                  name="palavrasChave"
                  rules={[
                    {
                      required: true,
                      message: 'Insira as palavras-chave',
                    },
                  ]}>
                  <Input placeholder="Insira as palavras e separe por vírgula" />
                </Form.Item>
              </Col>

              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit" onClick={() => setModalShowAnexo(true)}>
                  Salvar
                </button>
              </Col>
            </Row>
            
           {idProjeto ? <ModalAnexo
              show={modalShowAnexo}
              onHide={() => setModalShowAnexo(false)}
              idProjeto={idProjeto}
              etapaMembro={etapaMembro}
            /> : null}

            <ModalMembros
              show={modalShowMembros}
              onHide={() => setModalShowMembros(false)}
            />

            <ModalEdital
              show={modalShowEdital}
              onHide={() => setModalShowEdital(false)}
            />

            <ModalArea
              show={modalShowArea}
              onHide={() => setModalShowArea(false)}
            />

          </div>
        </Form>
      </main>
    </div>
  );
}
