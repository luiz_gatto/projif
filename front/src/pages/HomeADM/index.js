import styles from "../HomeADM/styles.module.scss";
import { Header } from "../../Components/HeaderADM";
import { Row, Col } from "react-bootstrap";

export default function Home() {
  return (
    <div className={styles.wrapper}>
      <Header />
      <main className={styles.homeContainer}>
        <img src="/logo.png" alt="logo" className={styles.logo} />
        <h1>Uma biblioteca dos projetos científicos acadêmicos</h1>
        <h1>do Instituto Federal do Sul de Minas</h1>

        <Row className={styles.menu}>
          <Col md={3} className={styles.itemMenu}>
            <div
              class="card  bg-success mb-3"
              onClick={(event) => {
                event.preventDefault();
                window.open("/#/listagem_projetosADM");
              }}
            >
              <Row>
                <Col md={3}>
                  <img src="/iconProjeto.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Projetos</h6>
                </Col>
              </Row>
            </div>
          </Col>

          <Col
            md={2}
            className={styles.itemMenu3}
            onClick={(event) => {
              event.preventDefault();
              window.open("/#/listagem_pessoasADM");
            }}
          >
            <div class="card  bg-success mb-3">
              <Row>
                <Col md={3}>
                  <img src="/iconPessoa.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Pessoas</h6>
                </Col>
              </Row>
            </div>
          </Col>

          <Col
            md={2}
            className={styles.itemMenu}
            onClick={(event) => {
              event.preventDefault();
              window.open("/#/listagem_comentariosADM");
            }}
          >
            <div class="card  bg-success mb-3">
              <Row>
                <Col md={3}>
                  <img src="/feed.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Feed</h6>
                </Col>
              </Row>
            </div>
          </Col>

          <Col
            md={2}
            className={styles.itemMenu}
            onClick={(event) => {
              event.preventDefault();
              window.open("/#/listagem_editaisADM");
            }}
          >
            <div class="card  bg-success mb-3">
              <Row>
                <Col md={3}>
                  <img src="/iconEdital.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Editais</h6>
                </Col>
              </Row>
            </div>
          </Col>
          <Col md={3} className={styles.itemMenu1}>
            <div
              class="card  bg-success mb-3"
              onClick={(event) => {
                event.preventDefault();
                window.open("/#/listagem_cursosADM");
              }}
            >
              <Row>
                <Col md={3}>
                  <img src="/iconCurso.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Cursos</h6>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </main>
    </div>
  );
}
