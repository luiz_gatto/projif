import styles from "./styles.module.scss";

import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div className={styles.wrapper}>
      <main className={styles.homeContainer}>
        <div md={12} className={styles.itemADM}>
          <div
            class="card  border-success mb-3"
            onClick={(event) => {
              event.preventDefault();
              window.open("/#/login");
            }}
          >
            <Row>
              <Col md={2}>
                <img src="/Login.png" alt="logo" />
              </Col>
              <Col md={10}>
                <h6 class="card-text text-success">Área Administrativa</h6>
              </Col>
            </Row>
          </div>
        </div>

        <img src="/logo.png" alt="logo" className={styles.logo} />
        <h1>Uma biblioteca dos projetos científicos acadêmicos</h1>
        <h1>do Instituto Federal do Sul de Minas</h1>

        <Row className={styles.menu}>
          <Col md={3} className={styles.itemMenu}>
            <div
              class="card  bg-success mb-3"
              onClick={(event) => {
                event.preventDefault();
                window.open("/#/listagem_projetos");
              }}
            >
              <Row>
                <Col md={3}>
                  <img src="/iconProjeto.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Projetos</h6>
                </Col>
              </Row>
            </div>
          </Col>
          <Col
            md={2}
            className={styles.itemMenu3}
            onClick={(event) => {
              event.preventDefault();
              window.open("/#/listagem_pessoas");
            }}
          >
            <div class="card  bg-success mb-3">
              <Row>
                <Col md={3}>
                  <img src="/iconPessoa.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Pessoas</h6>
                </Col>
              </Row>
            </div>
          </Col>
          <Col
            md={2}
            className={styles.itemMenu}
            onClick={(event) => {
              event.preventDefault();
              window.open("/#/listagem_comentarios");
            }}
          >
            <div class="card  bg-success mb-3">
              <Row>
                <Col md={3}>
                  <img src="/feed.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Feed</h6>
                </Col>
              </Row>
            </div>
          </Col>
          <Col
            md={2}
            className={styles.itemMenu}
            onClick={(event) => {
              event.preventDefault();
              window.open("/#/listagem_editais");
            }}
          >
            <div class="card  bg-success mb-3">
              <Row>
                <Col md={3}>
                  <img src="/iconEdital.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Editais</h6>
                </Col>
              </Row>
            </div>
          </Col>

          <Col md={3} className={styles.itemMenu1}>
            <div
              class="card  bg-success mb-3"
              onClick={(event) => {
                event.preventDefault();
                window.open("/#/listagem_cursos");
              }}
            >
              <Row>
                <Col md={3}>
                  <img src="/iconCurso.png" alt="logo" />
                </Col>
                <Col md={9}>
                  <h6 class="card-text text-white">Cursos</h6>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </main>
    </div>
  );
}
