import React, { useEffect, useState } from "react";
import LoginService from "../../services/loginService";
import { useParams } from "react-router-dom";
import {  message } from "antd";

const RedefinirSenha = (props) => {
  const { hash } = useParams();
  const [senha, setSenha] = useState(null);
  const [novaSenha, setNovaSenha] = useState(null);

  useEffect(() => {
    LoginService.validarHash(hash)
      .then(() => {
      })
      .catch(() => {
        message.error("Hash de validação invalida");
        window.location.href = "/#/login";
      });
  }, [hash]);

  const confirmar = () => {
    if (
      senha == null ||
      senha.length === 0 ||
      novaSenha == null ||
      novaSenha.length === 0
    ) {
        message.warning("Preencha todos os campos");
      return;
    }

    if (senha.length < 6 || novaSenha.length < 6) {
        message.warning("A senha deve ter 6 carasteres");
      return;
    }

    if (senha !== novaSenha) {
        message.warning("Repita corretamente a senha");
      return;
    }

    LoginService.redefinirSenha({ senha: senha, hash: hash }, hash)
      .then(() => {
        message.success("Senha alterada com sucesso");
        window.location.href = "/#/login";
      })
      .catch(() => {
        message.error("Hash de validação invalida");
        window.location.href = "/#/";
      });
  };

  return (
    <div className="container mt-5">
      <div className="row pt-5">
        <div className="col-12 text-center">
          <h1>Redefinir Senha</h1>
        </div>
        <label className="col-12 mt-1">
          Senha:
          <input
            type="password"
            value={senha}
            placeholder="Nova senha"
            className={`form-control ${
              senha !== null && senha.length < 6 ? "invalid" : ""
            }`}
            onChange={(e) => setSenha(e.target.value)}
          />
        </label>
        <label className="col-12">
          Nova senha:
          <input
            type="password"
            value={novaSenha}
            placeholder="Repita a senha"
            className={`form-control ${
              novaSenha !== null &&
              (novaSenha.length < 6 || novaSenha !== senha)
                ? "invalid"
                : ""
            }`}
            onChange={(e) => setNovaSenha(e.target.value)}
          />
        </label>
        <div className="col-md-4">
          <button className="btn btn-success" type="button" onClick={confirmar}>
            Confirmar
          </button>
        </div>
      </div>
    </div>
  );
};

export default RedefinirSenha;
