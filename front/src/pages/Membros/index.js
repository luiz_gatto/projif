import "antd/dist/antd.css"
import styles from '../Membros/styles.module.scss'
import React from 'react';
import MembroService from "../../services/membroService";
import ProjetoService from "../../services/projetoService";
import PessoaService from "../../services/pessoaService";
import CursoService from "../../services/cursoService";

import { Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react'
import { Form, message, Input, Select } from 'antd';
import { Link } from 'react-router-dom';

import { Header } from '../../Components/HeaderADM';
import { ModalPessoas } from '../../Components/ModalPessoas';
import { ModalCursos } from '../../Components/ModalCursos';

const { Option } = Select;

export default function CadastrarMembros() {

  const [projetos, setProjetos] = useState([]);
  const [projeto, setProjeto] = useState([]);

  const [pessoas, setPessoas] = useState([]);
  const [pessoa, setPessoa] = useState([]);

  const [cursos, setCursos] = useState([]);
  const [curso, setCurso] = useState([]);



  const [form] = Form.useForm();

  useEffect(() => {
    refresh();
    return () => {
    }
  }, [])

  async function refresh() {
    PessoaService.retrieveAllPessoas()
      .then(
        response => {
          setPessoas(response)
        }
      )
    CursoService.retrieveAllCursos()
      .then(
        response => {
          setCursos(response)
        }
      )
    ProjetoService.retrieveAllProjetos()
      .then(
        response => {
          setProjetos(response)
        }
      )
  }

  const onFinish = (values) => {

    values.pessoa = {
      idPessoa: pessoa
    };

    values.projeto = {
      idProjeto: projeto
    };

    if (values.tipo === "DISCENTE") {
      values.curso = {
        idCurso: curso
      };
    }

    form.resetFields();

    MembroService.saveMembro(values).then(() => {
      message.success("Membro incluído com sucesso!")
    })
      .catch((error) => {
        message.error("Cadastro não foi concluído!")
      });
  }

  const onFinishFailed = (erroInfo) => {
    console.log('Failed:', erroInfo)
  }

  function handleChangePessoa(value) {
    setPessoa(value);
  }

  function handleChangeCurso(value) {
    setCurso(value);
  }

  function handleChangeProjeto(value) {
    setProjeto(value);
  }

  const [modalShowPessoas, setModalShowPessoas] = React.useState(false);
  const [modalShowCursos, setModalShowCursos] = React.useState(false);

  return (
    <div className={styles.wrapper}>
      <Header />
      <main className={styles.App}>
        <Row>
          <Col md={11}>
            <h1>Cadastrar Membros</h1>
          </Col>
          <Col md={1}>
            <button button className={styles.buttonRecarregar} type="button" onClick={refresh}>
              <img src="/recarregar.png" alt="logo" />
            </button>
          </Col>
        </Row>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish} onFinishFailed={onFinishFailed}>

          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>

              <Col md={11} className={styles.field}>
                <Form.Item
                  label="Nome:"
                  name="pessoa.idPessoa"
                  rules={[
                    {
                      required: true,
                      message: 'Informe o nome',
                    },
                  ]}>
                  <Select onChange={handleChangePessoa} >
                    <Option value="" disabled selected>Selecione o nome</Option>
                    {pessoas.map((item) => {
                      return (
                        <Option key={item.idPessoa} value={item.idPessoa} >
                          {item.nome}
                        </Option>
                      )
                    })}
                  </Select>
                </Form.Item>
              </Col>

              <Col md={1}>
                <button className={styles.buttonNovo} type="button" onClick={() => setModalShowPessoas(true)}>
                  Novo
                </button>
              </Col>

              <Col md={11} className={styles.field}>
                <Form.Item
                  label="Projeto:"
                  name="projeto.idProjeto"
                  rules={[
                    {
                      required: true,
                      message: 'Informe o projeto',
                    },
                  ]}>
                  <Select onChange={handleChangeProjeto} >
                    <Option value="" disabled selected>Selecione o projeto</Option>
                    {projetos.map((item) => {
                      return (
                        <Option key={item.idProjeto} value={item.idProjeto} >
                          {item.titulo}
                        </Option>
                      )
                    })}
                  </Select>
                </Form.Item>
              </Col>

              <Col md={1}>

                <Link to="/projetos" target="_blank">
                  <button className={styles.buttonNovo} type="button">
                    Novo
                  </button>
                </Link>

              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Tipo:"
                  name="tipo"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o tipo',
                    },
                  ]}>
                  <select>
                    <option value="" disabled selected>Selecione o tipo de membro</option>
                    <option value="DISCENTE">Discente</option>
                    <option value="DOCENTE">Docente</option>
                    <option value="SERVIDOR">Servidor</option>
                    <option value="EXTERNO">Externo</option>
                  </select>
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Status:"
                  name="status"
                  rules={[
                    {
                      required: true,
                      message: 'Informe o status',
                    },
                  ]}>
                  <select>
                    <option value="" disabled selected>Selecione o status do membro</option>
                    <option value="ATIVO">Ativo</option>
                    <option value="INATIVO">Inativo</option>
                  </select>
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Modalidade:"
                  name="modalidade"
                  rules={[
                    {
                      required: true,
                      message: 'Informe a modalidade',
                    },
                  ]}>

                  <select>
                    <option value="" disabled selected>Selecione a modalidade</option>
                    <option value="">-</option>
                    <option value="BOLSISTA">Bolsista</option>
                    <option value="VOLUNTÁRIO">Voluntário(a)</option>
                    <option value="ORIENTADOR">Orientador(a)</option>
                  </select>
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Valor da bolsa:"
                  name="valorBolsa">
                  <Input placeholder="Insira o valor" type="number" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>

                <Form.Item
                  label="Curso:"
                  name="curso.idCurso"
                  rules={[
                    {
                      required: false,
                      message: 'Informe o curso',
                    },
                  ]}>
                  <Select onChange={handleChangeCurso} >
                    <Option value="" disabled selected>Selecione o curso</Option>

                    {cursos.map((item) => {
                      return (
                        <Option key={item.idCurso} value={item.idCurso} >
                          {item.nome}
                        </Option>
                      )
                    })}
                  </Select>
                </Form.Item>
              </Col>

              <Col md={1}>
                <button className={styles.buttonNovo} type="button" onClick={() => setModalShowCursos(true)}>
                  Novo
                </button>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Início no projeto:"
                  name="data_inicio"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a data de início',
                    },
                  ]}>
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Finalização no projeto:"
                  name="data_fim"
                >
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit" >
                  Salvar
                </button>
              </Col>
            </Row>

            <ModalPessoas
              show={modalShowPessoas}
              onHide={() => setModalShowPessoas(false)}
            />

            <ModalCursos
              show={modalShowCursos}
              onHide={() => setModalShowCursos(false)}
            />
          </div>
        </Form>
      </main>
    </div>
  );
}
