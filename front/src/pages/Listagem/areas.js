import styles from "../Listagem/styles.module.scss";
import areaConhecimentoService from "../../services/areaConhecimentoService";
import projetoService from "../../services/projetoService";
import PesquisaArea from "../../Components/PesquisaAreas";
import ListItem from "../../Components/ListItemAreas";

import { HeaderVisitante } from "../../Components/Header";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { Modal } from "react-bootstrap";

export default function Areas() {
  const [ativo, setAtivo] = useState(false);
  const handleClose = () => setAtivo(false);

  const [areas, setAreas] = useState([]);
  const [area, setArea] = useState([]);
  const [projetos, setProjetos] = useState([]);

  useEffect(() => {
    refreshAreas();
    return () => {};
  }, []);

  async function refreshAreas() {
    areaConhecimentoService.retrieveAllAreas().then((response) => {
      setAreas(response);
    });

    projetoService.retrieveAllProjetos().then((response) => {
      setProjetos(response);
    });
  }
  function chamarPagina(area) {
    setAtivo(true);
    setArea(area);
  }
  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>
            {area.codArea} - {area.area}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-center">
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Projetos</h5>
            </Col>
          </Row>
          <table className="table table-bordered table-hover text-center">
            <thead className="thead-light">
              <tr>
                <th scope="col">Título</th>
                <th scope="col">Edital</th>
                <th scope="col">Tipo</th>
                <th scope="col">Acesso</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {projetos.map((projeto) => (
                <tr>
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td>{projeto.titulo}</td>
                  ) : (
                    ""
                  )}
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td>
                      {projeto.edital &&
                        projeto.edital.numero +
                          "/" +
                          projeto.edital.ano +
                          " - " +
                          projeto.edital.descricao}
                    </td>
                  ) : (
                    ""
                  )}
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td>{projeto.tipo}</td>
                  ) : (
                    ""
                  )}
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td>{projeto.acesso}</td>
                  ) : (
                    ""
                  )}
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td
                      className={`${
                        projeto.status === "ANDAMENTO"
                          ? "text-success"
                          : "text-danger"
                      }`}
                    >
                      {projeto.status}
                    </td>
                  ) : (
                    ""
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>
      </Modal>

      <HeaderVisitante />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_areas">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaArea setAreas={setAreas} />
        <Row>
          <div className={styles.cursos}>
            {areas.map((area) => {
              return (
                <Col md={4} onClick={() => chamarPagina(area)}>
                  <ListItem
                    key={area.idArea}
                    codArea={area.codArea}
                    area={area.area}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
