import styles from "../Listagem/styles.module.scss";
import areaConhecimentoService from "../../services/areaConhecimentoService";
import projetoService from "../../services/projetoService";
import PesquisaArea from "../../Components/PesquisaAreas";
import ListItem from "../../Components/ListItemAreas";
import InputMask from "react-input-mask";

import { Header } from "../../Components/HeaderADM";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { Form, Input, message, Button } from "antd";
import { Modal } from "react-bootstrap";

const InputCod = (props) => (
  <InputMask
    mask="9.99.99.99-9"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o código"
  />
);

export default function Areas() {
  const [ativo, setAtivo] = useState(false);
  const handleClose = () => setAtivo(false);
  const [ativoAtualizar, setAtivoAtualizar] = useState(false);
  const handleCloseAtualizar = () => setAtivoAtualizar(false);
  const [ativoConfirmar, setAtivoConfirmar] = useState(false);
  const handleCloseConfirmar = () => setAtivoConfirmar(false);

  const [areas, setAreas] = useState([]);
  const [area, setArea] = useState([]);
  const [projetos, setProjetos] = useState([]);

  const [form] = Form.useForm();

  useEffect(() => {
    refreshAreas();
    return () => {};
  }, []);

  async function refreshAreas() {
    areaConhecimentoService.retrieveAllAreas().then((response) => {
      setAreas(response);
    });
    projetoService.retrieveAllProjetos().then((response) => {
      setProjetos(response);
    });
  }

  function chamarPagina(area) {
    setAtivo(true);
    setArea(area);
  }

  function chamarPaginaAtualizar(area) {
    handleClose();
    setAtivoAtualizar(true);
    setArea(area);
  }

  function confirmar(area) {
    handleCloseAtualizar();
    setAtivoConfirmar(true);
    setArea(area);
  }

  const excluirCadastro = (values) => {
    handleCloseConfirmar();
    areaConhecimentoService
      .deleteArea(values.idArea)
      .then(() => {
        refreshAreas();
        message.success("Cadastro apagado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não excluído!");
      });
  };

  const onFinish = (values) => {
    handleCloseAtualizar();
    form.resetFields();

    values.idArea = area.idArea;

    if (values.codArea == null) {
      values.codArea = area.codArea;
    }

    if (values.area == null) {
      values.area = area.area;
    }

    areaConhecimentoService
      .updateAreas(values)
      .then(() => {
        refreshAreas();
        message.success("Cadastro Atualizado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não atualizado!");
      });
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativoConfirmar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.ModalConfirmar}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseConfirmar}
          className={styles.titulo}
        >
          <Modal.Title>Confirmar Exclusão</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <h4>
            Certeza que deseja excluir a área <b>{area.area}</b> agora?
          </h4>
          <br />
          <h6 class="text-center">
            Todos os projetos relacionados serão excluídos imediatamente.
          </h6>
        </Modal.Body>
        <div className={styles.confirmar}>
          <Modal.Footer>
            <Button className={styles.btnNao} onClick={handleCloseConfirmar}>
              Não
            </Button>
            <Button
              className={styles.btnSim}
              onClick={() => excluirCadastro(area)}
            >
              Sim
            </Button>
          </Modal.Footer>
        </div>
      </Modal>

      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>
            {area.codArea} - {area.area}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-center">
          <div className={styles.atualizar}>
            <Button
              class="btn btn-success"
              md={4}
              onClick={() => chamarPaginaAtualizar(area)}
            >
              Atualizar Dados
            </Button>
          </div>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Projetos</h5>
            </Col>
          </Row>
          <table className="table table-bordered table-hover text-center">
            <thead className="thead-light">
              <tr>
                <th scope="col">Título</th>
                <th scope="col">Edital</th>
                <th scope="col">Tipo</th>
                <th scope="col">Acesso</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {projetos.map((projeto) => (
                <tr>
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td>{projeto.titulo}</td>
                  ) : (
                    ""
                  )}
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td>
                      {projeto.edital &&
                        projeto.edital.numero +
                          "/" +
                          projeto.edital.ano +
                          " - " +
                          projeto.edital.descricao}
                    </td>
                  ) : (
                    ""
                  )}
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td>{projeto.tipo}</td>
                  ) : (
                    ""
                  )}
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td>{projeto.acesso}</td>
                  ) : (
                    ""
                  )}
                  {projeto.areaConhecimento &&
                  projeto.areaConhecimento.idArea === area.idArea ? (
                    <td
                      className={`${
                        projeto.status === "ANDAMENTO"
                          ? "text-success"
                          : "text-danger"
                      }`}
                    >
                      {projeto.status}
                    </td>
                  ) : (
                    ""
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>
      </Modal>
      <Modal
        id="modCome"
        show={ativoAtualizar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseAtualizar}
          className={styles.titulo}
        >
          <Modal.Title>{area.area} - Atualizar Dados</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-center">
          <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
            <div className={styles.fieldsContainer}>
              <Row className={styles.fields}>
                <Col md={4} className={styles.field}>
                  <Form.Item label="Código da Área:" name="codArea">
                    <InputCod placeholder="Insira o código da área" />
                  </Form.Item>
                </Col>
                <Col md={8} className={styles.field}>
                  <Form.Item label="Área do Conhecimento:" name="area">
                    <Input placeholder="Insira a área" />
                  </Form.Item>
                </Col>
                <Col md={11}>
                  <button className={styles.buttonSubmit} type="submit">
                    Salvar
                  </button>
                </Col>
                <Col md={1}>
                  <Button
                    className={styles.buttonExcluir}
                    onClick={() => confirmar(area)}
                    type="submit"
                  >
                    <img src="/excluir.png" alt="excluir" />
                  </Button>
                </Col>
              </Row>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      <Header />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_areas">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaArea setAreas={setAreas} />
        <Row>
          <div className={styles.cursos}>
            {areas.map((area) => {
              return (
                <Col md={4} onClick={() => chamarPagina(area)}>
                  <ListItem
                    key={area.idArea}
                    codArea={area.codArea}
                    area={area.area}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
