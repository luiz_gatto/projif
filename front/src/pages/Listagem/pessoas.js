import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemPessoas";
import pessoaService from "../../services/pessoaService";
import membroService from "../../services/membroService";
import PesquisaPessoa from "../../Components/PesquisaPessoas";

import { HeaderVisitante } from "../../Components/Header";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { Modal } from "react-bootstrap";

export default function Pessoas() {
  const [ativo, setAtivo] = useState(false);
  const handleClose = () => setAtivo(false);

  const [pessoas, setPessoas] = useState([]);
  const [pessoa, setPessoa] = useState([]);

  const [membros, setMembros] = useState([]);

  useEffect(() => {
    refreshPessoas();
    return () => {};
  }, []);

  async function refreshPessoas() {
    pessoaService.retrieveAllPessoas().then((response) => {
      setPessoas(response);
    });
    membroService.retrieveAllMembros().then((response) => {
      setMembros(response);
    });
  }

  function chamarPagina(pessoa) {
    setAtivo(true);
    setPessoa(pessoa);
  }

  const formatarData = (date) => {
    const data = new Date(date);
    const dataFormatada = data.toLocaleDateString("pt-BR", { timeZone: "UTC" });
    return dataFormatada;
  };
  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{pessoa.nome}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Row>
            <Col md={6} className={styles.separadorPessoa}>
              <Row>
                <Col md={2}>
                  <img src="/email.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>E-mail: </h6>
                  <p>{pessoa.email}</p>
                </Col>
              </Row>
            </Col>

            <Col md={6} className={styles.separadorPessoa}>
              <Row>
                <Col md={2}>
                  <img src="/lattes.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>Currículo Lattes: </h6>
                  <p>{pessoa.lattes}</p>
                </Col>
              </Row>
            </Col>

            <Col md={6} className={styles.separadorPessoa}>
              <Row>
                <Col md={2}>
                  <img src="/telefone.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>Telefone: </h6>
                  <p>{pessoa.telefone}</p>
                </Col>
              </Row>
            </Col>

            <Col md={6} className={styles.separadorPessoa}>
              <Row>
                <Col md={2}>
                  <img src="/data.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>Data de Nascimento: </h6>
                  <p>
                    <p>{formatarData(pessoa.data_nascimento)}</p>
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Projetos </h5>
            </Col>
          </Row>
          <table className="table table-bordered table-hover text-center">
            <thead className="thead-light">
              <tr>
                <th scope="col">Projeto</th>
                <th scope="col">Membro</th>
                <th scope="col">Modalidade</th>
                <th scope="col">Curso</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {membros.map((membro) => (
                <tr>
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td>{membro.projeto && membro.projeto.titulo}</td>
                  ) : (
                    ""
                  )}
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td>{membro.tipo}</td>
                  ) : (
                    ""
                  )}
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td>{membro.modalidade}</td>
                  ) : (
                    ""
                  )}
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td>
                      {" "}
                      {(membro.curso && membro.curso.nome) === null
                        ? "N/A"
                        : membro.curso && membro.curso.nome}
                    </td>
                  ) : (
                    ""
                  )}
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td
                      className={`${
                        membro.status === "ATIVO"
                          ? "text-success"
                          : "text-danger"
                      }`}
                    >
                      {membro.status}
                    </td>
                  ) : (
                    ""
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>
      </Modal>

      <HeaderVisitante />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_pessoas">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaPessoa setPessoas={setPessoas} />
        <Row>
          <div className={styles.pessoas}>
            {pessoas.map((pessoa) => {
              return (
                <Col md={4} onClick={() => chamarPagina(pessoa)}>
                  <ListItem
                    key={pessoa.idPessoa}
                    nome={pessoa.nome}
                    email={pessoa.email}
                    lattes={pessoa.lattes}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
