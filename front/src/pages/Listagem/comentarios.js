import "bootstrap/dist/css/bootstrap.min.css";
import ListItem from "../../Components/ListItemComentarios";
import comentarioService from "../../services/comentarioService";
import projetoService from "../../services/projetoService";
import PesquisaComentarios from "../../Components/PesquisaComentarios";
import styles from "../Listagem/styles.module.scss";

import { useEffect, useState } from "react";
import { HeaderVisitante } from "../../Components/Header";
import { Row, Col } from "react-bootstrap";

export default function Comentarios() {
  const [comentarios, setComentarios] = useState([]);
  const [projetos, setProjetos] = useState([]);

  useEffect(() => {
    refresh();
    return () => {};
  });
  async function refresh() {
    comentarioService.retrieveAllComentarios().then((response) => {
      setComentarios(response);
    });
    projetoService.retrieveAllProjetos().then((response) => {
      setProjetos(response);
    });
  }

  return (
    <div className={styles.wrapper}>
      <HeaderVisitante />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_comentarios">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaComentarios setComentarios={setComentarios} />
        <Row>
          <div className={styles.comentarios}>
            {comentarios.map((comentario) => {
              return (
                <Col md={12} className={styles.item}>
                  {comentario.status === "PUBLICADO" ? (
                    <ListItem
                      key={comentario.idComentario}
                      titulo={comentario.projeto && comentario.projeto.titulo}
                      nome={comentario.nome}
                      email={comentario.email}
                      conteudo={comentario.conteudo}
                    />
                  ) : (
                    ""
                  )}
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
