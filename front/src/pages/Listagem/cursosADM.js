import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemCursos";
import CursoService from "../../services/cursoService";
import PesquisaCurso from "../../Components/PesquisaCursos";
import membroService from "../../services/membroService";

import { Form, Input, message, Button } from "antd";
import { Modal } from "react-bootstrap";
import { Header } from "../../Components/HeaderADM";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";

export default function Cursos() {
  const [ativo, setAtivo] = useState(false);
  const [ativoAtualizar, setAtivoAtualizar] = useState(false);
  const handleCloseAtualizar = () => setAtivoAtualizar(false);
  const handleClose = () => setAtivo(false);

  const [ativoConfirmar, setAtivoConfirmar] = useState(false);
  const handleCloseConfirmar = () => setAtivoConfirmar(false);

  const [cursos, setCursos] = useState([]);
  const [curso, setCurso] = useState([]);
  const [membros, setMembros] = useState([]);

  const [form] = Form.useForm();

  useEffect(() => {
    refreshCursos();
    return () => {};
  }, []);

  async function refreshCursos() {
    CursoService.retrieveAllCursos().then((response) => {
      setCursos(response);
    });
    membroService.retrieveAllMembros().then((response) => {
      setMembros(response);
    });
  }

  const onFinish = (values) => {
    handleCloseAtualizar();
    form.resetFields();

    values.idCurso = curso.idCurso;

    if (values.nome == null) {
      values.nome = curso.nome;
    }

    if (values.modalidade == null) {
      values.modalidade = curso.modalidade;
    }

    if (values.campus == null) {
      values.campus = curso.campus;
    }

    CursoService.updateCursos(values)
      .then(() => {
        refreshCursos();
        message.success("Cadastro Atualizado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não atualizado!");
      });
  };

  function chamarPagina(curso) {
    setAtivo(true);
    setCurso(curso);
  }

  function chamarPaginaAtualizar(curso) {
    handleClose();
    setAtivoAtualizar(true);
    setCurso(curso);
  }

  function confirmar(curso) {
    handleCloseAtualizar();
    setAtivoConfirmar(true);
    setCurso(curso);
  }

  const excluirCadastro = (values) => {
    handleCloseConfirmar();
    CursoService.deleteCursos(values.idCurso)
      .then(() => {
        refreshCursos();
        message.success("Cadastro apagado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não excluído!");
      });
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativoConfirmar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.ModalConfirmar}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseConfirmar}
          className={styles.titulo}
        >
          <Modal.Title>Confirmar Exclusão</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <h4>
            Certeza que deseja excluir o curso <b>{curso.nome}</b> agora?
          </h4>
          <br />
          <h6 class="text-center">
            Todos os membros relacionados serão excluídos imediatamente.
          </h6>
        </Modal.Body>
        <div className={styles.confirmar}>
          <Modal.Footer>
            <Button className={styles.btnNao} onClick={handleCloseConfirmar}>
              Não
            </Button>
            <Button
              className={styles.btnSim}
              onClick={() => excluirCadastro(curso)}
            >
              Sim
            </Button>
          </Modal.Footer>
        </div>
      </Modal>

      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{curso.nome}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-center">
          <Row>
            <Col md={6} className={styles.separadorCurso}>
              <Row>
                <Col md={3}>
                  <img src="/curso.png" alt="icon" />
                </Col>
                <Col md={9}>
                  <h6 className={styles.campo}>Modalidade </h6>
                  <p>{curso.modalidade}</p>
                </Col>
              </Row>
            </Col>

            <Col md={6} className={styles.separadorCurso}>
              <Row>
                <Col md={3}>
                  <img src="/campus.png" alt="icon" />
                </Col>
                <Col md={9}>
                  <h6 className={styles.campo}>Campus </h6>
                  <p>{curso.campus}</p>
                </Col>
              </Row>
            </Col>
            <div className={styles.atualizar}>
              <Button
                class="btn btn-success"
                md={4}
                onClick={() => chamarPaginaAtualizar(curso)}
              >
                Atualizar Dados
              </Button>
            </div>
          </Row>

          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Membros Relacionados </h5>
            </Col>
          </Row>
        </Modal.Body>

        <table className="table table-bordered table-hover text-center">
          <thead className="thead-light">
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Projeto</th>
              <th scope="col">Modalidade</th>
              <th scope="col">Tipo</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
            {membros.map((membro) => (
              <tr>
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td>{membro.pessoa && membro.pessoa.nome}</td>
                ) : (
                  ""
                )}
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td>{membro.projeto && membro.projeto.titulo}</td>
                ) : (
                  ""
                )}
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td>{membro.modalidade}</td>
                ) : (
                  ""
                )}
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td>{membro.tipo}</td>
                ) : (
                  ""
                )}
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td
                    className={`${
                      membro.status === "ATIVO" ? "text-success" : "text-danger"
                    }`}
                  >
                    {membro.status}
                  </td>
                ) : (
                  ""
                )}
              </tr>
            ))}
          </tbody>
        </table>
      </Modal>

      <Modal
        id="modCome"
        show={ativoAtualizar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseAtualizar}
          className={styles.titulo}
        >
          <Modal.Title>{curso.nome} - Atualizar Dados</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-enter">
          <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
            <Row>
              <Col md={4} className={styles.separadorCursoAtualizar}>
                <Row>
                  <Col md={2}>
                    <img src="/curso.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Curso </h6>
                    <p>{curso.nome}</p>
                    <Form.Item name="nome">
                      <Input placeholder="Atualize o nome do curso" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col md={4} className={styles.separadorCursoAtualizar}>
                <Row>
                  <Col md={2}>
                    <img src="/projeto.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Modalidade </h6>
                    <p>{curso.modalidade}</p>
                    <Form.Item name="modalidade">
                      <select>
                        <option value="" disabled selected>
                          Selecione a modalidade
                        </option>
                        <option value="SUPERIOR">Superior</option>
                        <option value="TÉCNICO">Técnico</option>
                      </select>
                    </Form.Item>
                  </Col>
                </Row>
              </Col>

              <Col md={4} className={styles.separadorCursoAtualizar}>
                <Row>
                  <Col md={2}>
                    <img src="/campus.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Campus </h6>
                    <p>{curso.campus}</p>
                    <Form.Item name="campus">
                      <Input placeholder="Insira o campus" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col md={11}>
                <button className={styles.buttonSubmit} type="submit">
                  Salvar
                </button>
              </Col>
              <Col md={1}>
                <Button
                  className={styles.buttonExcluir}
                  onClick={() => confirmar(curso)}
                  type="submit"
                >
                  <img src="/excluir.png" alt="excluir" />
                </Button>
              </Col>
            </Row>
          </Form>
        </Modal.Body>
      </Modal>

      <Header />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_cursosADM">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaCurso setCursos={setCursos} />
        <Row>
          <div className={styles.cursos}>
            {cursos.map((curso) => {
              return (
                <Col md={4} onClick={() => chamarPagina(curso)}>
                  <ListItem
                    key={curso.idCurso}
                    nome={curso.nome}
                    modalidade={curso.modalidade}
                    campus={curso.campus}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
