import "antd/dist/antd.css";
import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemProjetos";
import projetoService from "../../services/projetoService";
import membroService from "../../services/membroService";
import comentarioService from "../../services/comentarioService";
import PesquisaProjetos from "../../Components/PesquisaProjetos";
import AnexoService from "../../services/anexoService";
import { saveAs } from "file-saver";

import { Modal } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import { HeaderVisitante } from "../../Components/Header";
import { Form, message, Input } from "antd";

export default function Projetos() {
  const [ativo, setAtivo] = useState(false);
  const handleClose = () => setAtivo(false);
  const [projetos, setProjetos] = useState([]);
  const [projeto, setProjeto] = useState([]);
  const [membros, setMembros] = useState([]);
  const [comentarios, setComentarios] = useState([]);

  const [formComentario] = Form.useForm();

  const download = (idProj) => {
    AnexoService.download(idProj).then((arquivo) => {
      saveAs(arquivo, idProj.nome);
    });
  };

  useEffect(() => {
    refreshProjetos();
    return () => {};
  }, []);

  async function refreshProjetos() {
    projetoService.retrieveAllProjetos().then((response) => {
      setProjetos(response);
    });

    membroService.retrieveAllMembros().then((response) => {
      setMembros(response);
    });
    comentarioService.retrieveAllComentarios().then((response) => {
      setComentarios(response);
    });
  }

  function chamarPagina(projeto) {
    setAtivo(true);
    setProjeto(projeto);
  }

  const formatarData = (date) => {
    const data = new Date(date);
    const dataFormatada = data.toLocaleDateString("pt-BR", { timeZone: "UTC" });
    return dataFormatada;
  };

  const onFinish = (values) => {
    formComentario.resetFields();

    values.projeto = projeto;
    values.status = "PUBLICADO";

    comentarioService
      .saveComentarios(values)
      .then(() => {
        refreshProjetos();
        message.success("Comentário publicado com sucesso!");
      })
      .catch((error) => {
        message.error("Comentário não publicado!");
      });
  };

  const onFinishFailed = (erroInfo) => {
    console.log("Failed:", erroInfo);
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{projeto.titulo}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-justify">
          <Row className={styles.separador}>
            <Col md={12}>
              <h6 className={styles.campo}>Edital: </h6>
              <p>
                {projeto.edital &&
                  projeto.edital.numero +
                    "/" +
                    projeto.edital.ano +
                    " - " +
                    projeto.edital.descricao}
              </p>
            </Col>
          </Row>
          <Row className={styles.separador}>
            <Col md={9}>
              <h6 className={styles.campo}>Área do Conhecimento: </h6>
              <p>
                {projeto.areaConhecimento &&
                  projeto.areaConhecimento.codArea +
                    " - " +
                    projeto.areaConhecimento.area}
              </p>
            </Col>
            <Col md={3}>
              <h6 className={styles.campo}>Status:</h6>
              <p>{projeto.status}</p>
            </Col>
          </Row>
          <Row className={styles.separador}>
            <Col md={3}>
              <h6 className={styles.campo}>Início do Projeto: </h6>
              <p>{formatarData(projeto.data_inicio)}</p>
            </Col>
            <Col md={3}>
              <h6 className={styles.campo}>Final do Projeto: </h6>
              <p>{formatarData(projeto.data_fim)}</p>
            </Col>
            <Col md={2}>
              <h6 className={styles.campo}>Acesso: </h6>
              <p>{projeto.acesso}</p>
            </Col>
            <Col md={2}>
              <h6 className={styles.campo}>Tipo:</h6>
              <p> {projeto.tipo}</p>
            </Col>
            <Col md={3} className={styles.download}>
              <h6 className={styles.campo}>Anexos:</h6>
              {projeto.anexos && projeto.anexos.length > 0
                ? projeto.anexos.map((anexo) => {
                    return (
                      <>
                        <span>{anexo.nome}</span>
                        <span
                          onClick={() => {
                            AnexoService.download(anexo).then((response) => {
                              saveAs(response, anexo.nome);
                            });
                          }}
                        >
                          <img src="/download.png" alt="icon" />
                        </span>
                      </>
                    );
                  })
                : null}
            </Col>
          </Row>

          <Row></Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Resumo </h5>
              <p>{projeto.resumo}</p>
            </Col>
          </Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Membros </h5>
            </Col>
          </Row>

          <table className="table table-bordered table-hover text-center">
            <thead className="thead-light">
              <tr>
                <th scope="col">Nome</th>
                <th scope="col">Tipo</th>
                <th scope="col">Modalidade</th>
                <th scope="col">Curso</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {membros.map((membro) => (
                <tr>
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td>{membro.pessoa && membro.pessoa.nome}</td>
                  ) : (
                    ""
                  )}
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td>{membro.tipo}</td>
                  ) : (
                    ""
                  )}
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td>{membro.modalidade}</td>
                  ) : (
                    ""
                  )}
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td>
                      {" "}
                      {(membro.curso && membro.curso.nome) === null
                        ? "N/A"
                        : membro.curso && membro.curso.nome}
                    </td>
                  ) : (
                    ""
                  )}
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td
                      className={`${
                        membro.status === "ATIVO"
                          ? "text-success"
                          : "text-danger"
                      }`}
                    >
                      {membro.status}
                    </td>
                  ) : (
                    ""
                  )}
                </tr>
              ))}
            </tbody>
          </table>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Comentários </h5>
            </Col>
          </Row>

          <Row className={styles.comentario}>
            <div class="card">
              {comentarios.map((comentario) => (
                <Col md={12}>
                  {comentario.projeto &&
                  comentario.projeto.idProjeto === projeto.idProjeto ? (
                    <h6 class="card-header">
                      <img src="/pessoa.png" alt="icon" />
                      {comentario.nome}
                    </h6>
                  ) : (
                    ""
                  )}

                  {comentario.projeto &&
                  comentario.projeto.idProjeto === projeto.idProjeto ? (
                    <div class="card-body">
                      <p class="card-text">{comentario.conteudo}</p>
                    </div>
                  ) : (
                    ""
                  )}
                </Col>
              ))}
              <h6 class="card-header">
                <img src="/comentario.png" alt="icon" />
                Tem algo a dizer sobre esse projeto? Fique a vontade!
              </h6>
            </div>
          </Row>
          <Form
            form={formComentario}
            className={styles.formComentario}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              name="conteudo"
              rules={[
                {
                  required: true,
                  message: "Insira o seu comentário",
                },
              ]}
            >
              <textarea
                class="form-control"
                rows="3"
                maxlength="250"
                placeholder="Digite seu comentário aqui..."
                name="conteudo"
              />
            </Form.Item>

            <Row className={styles.campos}>
              <Col md={5}>
                <Form.Item
                  label="Informe seu nome:"
                  name="nome"
                  rules={[
                    {
                      required: true,
                      message: "Insira o seu nome!",
                    },
                  ]}
                >
                  <Input placeholder="Insira o seu melhor email" />
                </Form.Item>
              </Col>
              <Col md={7}>
                <Form.Item
                  label="Informe seu email:"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: "Insira o seu email!",
                    },
                  ]}
                >
                  <Input placeholder="Insira o seu email" />
                </Form.Item>
              </Col>
            </Row>

            <button className={styles.buttonSubmit} type="submit">
              Publicar
            </button>
          </Form>
        </Modal.Body>
      </Modal>
      <HeaderVisitante />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_projetos">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaProjetos setProjetos={setProjetos} />

        <Row>
          <div className={styles.projetos}>
            {projetos.map((projeto) => {
              return (
                <Col md={12} onClick={() => chamarPagina(projeto)}>
                  {projeto.acesso === "PÚBLICO" ? (
                    <ListItem
                      className={styles.item}
                      key={projeto.idProjeto}
                      titulo={projeto.titulo}
                      edital={
                        projeto.edital &&
                        projeto.edital.numero +
                          "/" +
                          projeto.edital.ano +
                          " - " +
                          projeto.edital.descricao
                      }
                      area={
                        projeto.areaConhecimento &&
                        projeto.areaConhecimento.codArea +
                          " - " +
                          projeto.areaConhecimento.area
                      }
                      tipo={projeto.tipo}
                    />
                  ) : (
                    ""
                  )}
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
