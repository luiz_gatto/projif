import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemPessoas";
import pessoaService from "../../services/pessoaService";
import membroService from "../../services/membroService";
import PesquisaPessoa from "../../Components/PesquisaPessoas";
import PessoaService from "../../services/pessoaService";
import InputMask from "react-input-mask";

import { Form, Input, message, Button } from "antd";
import { Modal } from "react-bootstrap";
import { Header } from "../../Components/HeaderADM";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";

const InputTel = (props) => (
  <InputMask
    mask="(99)99999-9999"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o número de telefone"
  />
);

const InputCPF = (props) => (
  <InputMask
    mask="999.999.999-99"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o CPF"
  />
);

const InputCEP = (props) => (
  <InputMask
    mask="99999-999"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o CEP"
  />
);

export default function Pessoas() {
  const [ativo, setAtivo] = useState(false);
  const [ativoAtualizar, setAtivoAtualizar] = useState(false);
  const [ativoConfirmar, setAtivoConfirmar] = useState(false);
  const handleClose = () => setAtivo(false);
  const handleCloseAtualizar = () => setAtivoAtualizar(false);
  const handleCloseConfirmar = () => setAtivoConfirmar(false);
  const [pessoas, setPessoas] = useState([]);
  const [pessoa, setPessoa] = useState([]);

  const [membros, setMembros] = useState([]);

  useEffect(() => {
    refreshPessoas();
    return () => {};
  }, []);

  const [form] = Form.useForm();

  const onFinish = (values) => {
    handleCloseAtualizar();
    form.resetFields();

    values.idPessoa = pessoa.idPessoa;

    if (values.nome == null) {
      values.nome = pessoa.nome;
    }
    if (values.cpf == null) {
      values.cpf = pessoa.cpf;
    }
    if (values.email == null) {
      values.email = pessoa.email;
    }
    if (values.telefone == null) {
      values.telefone = pessoa.telefone;
    }
    if (values.lattes == null) {
      values.lattes = pessoa.lattes;
    }
    if (values.data_nascimento == null) {
      values.data_nascimento = pessoa.data_nascimento;
    }
    if (values.logradouro == null) {
      values.logradouro = pessoa.logradouro;
    }
    if (values.numero == null) {
      values.numero = pessoa.numero;
    }
    if (values.cidade == null) {
      values.cidade = pessoa.cidade;
    }
    if (values.bairro == null) {
      values.bairro = pessoa.bairro;
    }
    if (values.estado == null) {
      values.estado = pessoa.estado;
    }
    if (values.uf == null) {
      values.uf = pessoa.uf;
    }
    if (values.cep == null) {
      values.cep = pessoa.cep;
    }

    PessoaService.updatePessoas(values)
      .then(() => {
        refreshPessoas();
        message.success("Cadastro Atualizado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não atualizado!");
      });
  };

  const excluirCadastro = (values) => {
    handleCloseConfirmar();
    PessoaService.deletePessoas(values.idPessoa)
      .then(() => {
        refreshPessoas();
        message.success("Cadastro apagado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não excluído!");
      });
  };

  function confirmar(pessoa) {
    handleCloseAtualizar();
    setAtivoConfirmar(true);
    setPessoa(pessoa);
  }

  async function refreshPessoas() {
    pessoaService.retrieveAllPessoas().then((response) => {
      setPessoas(response);
    });
    membroService.retrieveAllMembros().then((response) => {
      setMembros(response);
    });
  }

  function chamarPagina(pessoa) {
    setAtivo(true);
    setPessoa(pessoa);
  }

  function chamarPaginaAtualizar(pessoa) {
    handleClose();
    setAtivoAtualizar(true);
    setPessoa(pessoa);
  }
  const formatarData = (date) => {
    const data = new Date(date);
    const dataFormatada = data.toLocaleDateString("pt-BR", { timeZone: "UTC" });
    return dataFormatada;
  };
  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativoConfirmar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.ModalConfirmar}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseConfirmar}
          className={styles.titulo}
        >
          <Modal.Title>Confirmar Exclusão</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <h4>
            Certeza que deseja excluir o cadastro de <b>{pessoa.nome}</b> agora?
          </h4>
          <br />
          <h6 class="text-center">
            Todos os membros relacionados serão excluídos imediatamente.
          </h6>
        </Modal.Body>
        <div className={styles.confirmar}>
          <Modal.Footer>
            <Button className={styles.btnNao} onClick={handleCloseConfirmar}>
              Não
            </Button>

            <Button
              className={styles.btnSim}
              onClick={() => excluirCadastro(pessoa)}
            >
              Sim
            </Button>
          </Modal.Footer>
        </div>
      </Modal>

      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{pessoa.nome}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Row>
            <Col md={6} className={styles.separadorPessoa}>
              <Row>
                <Col md={2}>
                  <img src="/email.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>E-mail: </h6>
                  <p>{pessoa.email}</p>
                </Col>
              </Row>
            </Col>

            <Col md={6} className={styles.separadorPessoa}>
              <Row>
                <Col md={2}>
                  <img src="/lattes.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>Currículo Lattes: </h6>
                  <p>{pessoa.lattes}</p>
                </Col>
              </Row>
            </Col>

            <Col md={6} className={styles.separadorPessoa}>
              <Row>
                <Col md={2}>
                  <img src="/telefone.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>Telefone: </h6>
                  <p>{pessoa.telefone}</p>
                </Col>
              </Row>
            </Col>

            <Col md={6} className={styles.separadorPessoa}>
              <Row>
                <Col md={2}>
                  <img src="/data.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>Data de Nascimento: </h6>
                  <p>
                    <p>{formatarData(pessoa.data_nascimento)}</p>
                  </p>
                </Col>
              </Row>
            </Col>

            <Row className={styles.separadorTitulo}>
              <Col md={12}>
                <h5>Endereço</h5>
              </Col>
            </Row>

            <Col md={2} className={styles.separadorPessoa}>
              <h6 className={styles.campo}>Logradouro: </h6>
              <p>{pessoa.logradouro}</p>
            </Col>
            <Col md={1} className={styles.separadorPessoa}>
              <h6 className={styles.campo}>Nº: </h6>
              <p>{pessoa.numero}</p>
            </Col>
            <Col md={2} className={styles.separadorPessoa}>
              <h6 className={styles.campo}>Bairro: </h6>
              <p>
                <p>{pessoa.bairro}</p>
              </p>
            </Col>
            <Col md={2} className={styles.separadorPessoa}>
              <h6 className={styles.campo}>Cidade: </h6>
              <p>
                <p>{pessoa.cidade}</p>
              </p>
            </Col>
            <Col md={2} className={styles.separadorPessoa}>
              <h6 className={styles.campo}>CEP: </h6>
              <p>
                <p>{pessoa.cep}</p>
              </p>
            </Col>
            <Col md={2} className={styles.separadorPessoa}>
              <h6 className={styles.campo}>Estado: </h6>
              <p>
                <p>{pessoa.estado}</p>
              </p>
            </Col>
            <Col md={1} className={styles.separadorPessoa}>
              <h6 className={styles.campo}>UF: </h6>
              <p>
                <p>{pessoa.uf}</p>
              </p>
            </Col>

            <div className={styles.atualizar}>
              <Button
                class="btn btn-success"
                md={4}
                onClick={() => chamarPaginaAtualizar(pessoa)}
              >
                Atualizar Dados
              </Button>
            </div>
          </Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Projetos </h5>
            </Col>
          </Row>
          <table className="table table-bordered table-hover text-center">
            <thead className="thead-light">
              <tr>
                <th scope="col">Projeto</th>
                <th scope="col">Membro</th>
                <th scope="col">Modalidade</th>
                <th scope="col">Curso</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {membros.map((membro) => (
                <tr>
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td>{membro.projeto && membro.projeto.titulo}</td>
                  ) : (
                    ""
                  )}
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td>{membro.tipo}</td>
                  ) : (
                    ""
                  )}
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td>{membro.modalidade}</td>
                  ) : (
                    ""
                  )}
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td>
                      {" "}
                      {(membro.curso && membro.curso.nome) === null
                        ? "N/A"
                        : membro.curso && membro.curso.nome}
                    </td>
                  ) : (
                    ""
                  )}
                  {membro.pessoa &&
                  membro.pessoa.idPessoa === pessoa.idPessoa ? (
                    <td
                      className={`${
                        membro.status === "ATIVO"
                          ? "text-success"
                          : "text-danger"
                      }`}
                    >
                      {membro.status}
                    </td>
                  ) : (
                    ""
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>
      </Modal>

      <Modal
        id="modCome"
        show={ativoAtualizar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseAtualizar}
          className={styles.titulo}
        >
          <Modal.Title>{pessoa.nome} - Atualizar Dados</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
            <Row>
              <Col md={6} className={styles.separadorPessoa}>
                <Row>
                  <Col md={2}>
                    <img src="/pessoa.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Nome: </h6>
                    <p>{pessoa.nome}</p>
                    <Form.Item name="nome">
                      <Input placeholder="Atualize o nome" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col md={6} className={styles.separadorPessoa}>
                <Row>
                  <Col md={2}>
                    <img src="/pessoa.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>CPF: </h6>
                    <p>{pessoa.cpf}</p>
                    <Form.Item name="cpf">
                      <InputCPF placeholder="Atualize o CPF" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col md={6} className={styles.separadorPessoa}>
                <Row>
                  <Col md={2}>
                    <img src="/email.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>E-mail: </h6>
                    <p>{pessoa.email}</p>
                    <Form.Item name="email">
                      <Input placeholder="Atualize o email" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>

              <Col md={6} className={styles.separadorPessoa}>
                <Row>
                  <Col md={2}>
                    <img src="/lattes.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Currículo Lattes: </h6>
                    <p>{pessoa.lattes}</p>
                    <Form.Item name="lattes">
                      <Input placeholder="Atualize o currículo Lattes" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>

              <Col md={6} className={styles.separadorPessoa}>
                <Row>
                  <Col md={2}>
                    <img src="/telefone.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Telefone: </h6>
                    <p>{pessoa.telefone}</p>
                    <Form.Item name="telefone">
                      <InputTel />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>

              <Col md={6} className={styles.separadorPessoa}>
                <Row>
                  <Col md={2}>
                    <img src="/data.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Data de Nascimento: </h6>
                    <p>
                      <p>{formatarData(pessoa.data_nascimento)}</p>
                    </p>
                    <Form.Item name="data_nascimento">
                      <Input type="date" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>

              <Col md={12} className={styles.separadorEndereco}>
                <Row>
                  <Col md={1}>
                    <img src="/endereco.png" alt="icon" />
                  </Col>
                  <Col md={11}>
                    <h4 className={styles.campo}>Endereço</h4>
                  </Col>
                </Row>
              </Col>

              <Col md={12} className={styles.field}>
                <Form.Item label="Logradouro:" name="logradouro">
                  <Input placeholder="Insira o endereço" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item label="Nº:" name="numero">
                  <Input placeholder="Insira o número" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item label="Bairro:" name="bairro">
                  <Input placeholder="Insira o bairro" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item label="Cidade:" name="cidade">
                  <Input placeholder="Insira a cidade" />
                </Form.Item>
              </Col>
              <Col md={4} className={styles.field}>
                <Form.Item label="CEP:" name="cep">
                  <InputCEP />
                </Form.Item>
              </Col>
              <Col md={5} className={styles.field}>
                <Form.Item label="Estado:" name="estado">
                  <Input placeholder="Insira o estado" />
                </Form.Item>
              </Col>
              <Col md={3} className={styles.field}>
                <Form.Item label="UF:" name="uf">
                  <Input maxLength="2" placeholder="Insira a UF" />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col md={11}>
                <button className={styles.buttonSubmit} type="submit">
                  Salvar
                </button>
              </Col>
              <Col md={1}>
                <Button
                  className={styles.buttonExcluir}
                  onClick={() => confirmar(pessoa)}
                  type="submit"
                >
                  <img src="/excluir.png" alt="excluir" />
                </Button>
              </Col>
            </Row>
          </Form>
        </Modal.Body>
      </Modal>

      <Header />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_pessoasADM">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaPessoa setPessoas={setPessoas} />
        <Row>
          <div className={styles.pessoas}>
            {pessoas.map((pessoa) => {
              return (
                <Col md={4} onClick={() => chamarPagina(pessoa)}>
                  <ListItem
                    key={pessoa.idPessoa}
                    nome={pessoa.nome}
                    email={pessoa.email}
                    lattes={pessoa.lattes}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
