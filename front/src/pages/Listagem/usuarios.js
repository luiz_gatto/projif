import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemUsuarios";
import usuarioService from "../../services/usuarioService";
import PesquisaUsuario from "../../Components/PesquisaUsuarios";
import { message, Button } from "antd";
import { Header } from "../../Components/HeaderADM";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { Modal } from "react-bootstrap";

export default function Usuarios() {
  const [usuarios, setUsuarios] = useState([]);
  const [usuario, setUsuario] = useState([]);
  const [ativo, setAtivo] = useState(false);
  const [ativoConfirmar, setAtivoConfirmar] = useState(false);
  const handleClose = () => setAtivo(false);
  const handleCloseConfirmar = () => setAtivoConfirmar(false);

  useEffect(() => {
    refreshUsuarios();
    return () => {};
  }, []);

  async function refreshUsuarios() {
    usuarioService.retrieveAllUsuarios().then((response) => {
      setUsuarios(response);
    });
  }

  function chamarPagina(usuario) {
    setAtivo(true);
    setUsuario(usuario);
  }

  function confirmar(usuario) {
    handleClose();
    setAtivoConfirmar(true);
    setUsuario(usuario);
  }

  const excluirCadastro = (values) => {
    handleCloseConfirmar();
    usuarioService
      .deleteUsuario(values.idUsuario)
      .then(() => {
        refreshUsuarios();
        message.success("Cadastro apagado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não excluído!");
      });
  };

  const RedefinirSenha = (values) => {
    message.success("Link enviado para o email cadastrado!");
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativoConfirmar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.ModalConfirmar}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseConfirmar}
          className={styles.titulo}
        >
          <Modal.Title>Confirmar Exclusão</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <h4>
            Deseja excluir o usuário <b>{usuario.nome}</b> agora?
          </h4>
        </Modal.Body>
        <div className={styles.confirmar}>
          <Modal.Footer>
            <Button className={styles.btnNao} onClick={handleCloseConfirmar}>
              Não
            </Button>

            <Button
              className={styles.btnSim}
              onClick={() => excluirCadastro(usuario)}
            >
              Sim
            </Button>
          </Modal.Footer>
        </div>
      </Modal>

      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{usuario.nome}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Row>
            <Col md={6} className={styles.separadorUsuario1}>
              <Row>
                <Col md={2}>
                  <img src="/pessoa.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>Nome: </h6>
                  <p>{usuario.nome}</p>
                </Col>
              </Row>
            </Col>
            <Col md={6} className={styles.separadorUsuario2}>
              <Row>
                <Col md={2}>
                  <img src="/email.png" alt="icon" />
                </Col>
                <Col md={10}>
                  <h6 className={styles.campo}>E-mail: </h6>
                  <p>{usuario.email}</p>
                </Col>
              </Row>
            </Col>

            <Col md={11}>
              <button
                className={styles.buttonRedefinirSenha}
                onClick={() => RedefinirSenha(usuario)}
                type="submit"
              >
                Redefinir senha
              </button>
            </Col>
            <Col md={1}>
              <Button
                className={styles.buttonExcluir}
                onClick={() => confirmar(usuario)}
                type="submit"
              >
                <img src="/excluir.png" alt="excluir" />
              </Button>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>

      <Header />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_usuarios">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaUsuario setUsuario={setUsuarios} />
        <Row>
          <div className={styles.usuarios}>
            {usuarios.map((usuario) => {
              return (
                <Col md={4} onClick={() => chamarPagina(usuario)}>
                  <ListItem
                    key={usuario.idUsuario}
                    nome={usuario.nome}
                    email={usuario.email}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
