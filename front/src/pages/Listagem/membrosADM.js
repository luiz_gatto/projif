import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemMembros";
import membroService from "../../services/membroService";
import pessoaService from "../../services/pessoaService";
import PesquisaMembros from "../../Components/PesquisaMembros";
import cursoService from "../../services/cursoService";

import { Header } from "../../Components/HeaderADM";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { Form, message, Input, Button } from "antd";
import { Modal } from "react-bootstrap";

export default function Membros() {
  const [ativo, setAtivo] = useState(false);
  const handleClose = () => setAtivo(false);
  const [ativoAtualizar, setAtivoAtualizar] = useState(false);
  const handleCloseAtualizar = () => setAtivoAtualizar(false);
  const [ativoConfirmar, setAtivoConfirmar] = useState(false);
  const handleCloseConfirmar = () => setAtivoConfirmar(false);

  const [membro, setMembro] = useState([]);
  const [membros, setMembros] = useState([]);
  const [pessoas, setPessoas] = useState([]);
  const [cursos, setCursos] = useState([]);

  const [form] = Form.useForm();

  useEffect(() => {
    refreshMembros();
  });

  async function refreshMembros() {
    membroService.retrieveAllMembros().then((response) => {
      setMembros(response);
    });
    pessoaService.retrieveAllPessoas().then((response) => {
      setPessoas(response);
    });
    cursoService.retrieveAllCursos().then((response) => {
      setCursos(response);
    });
  }

  function chamarPagina(membro) {
    setAtivo(true);
    setMembro(membro);
  }

  function chamarPaginaAtualizar(membro) {
    handleClose();
    setAtivoAtualizar(true);
    setMembro(membro);
  }

  function confirmar(membro) {
    handleCloseAtualizar();
    setAtivoConfirmar(true);
    setMembro(membro);
  }

  const excluirCadastro = (values) => {
    handleCloseConfirmar();
    membroService
      .deleteMembro(values.idMembro)
      .then(() => {
        refreshMembros();
        message.success("Cadastro apagado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não excluído!");
      });
  };

  const onFinish = (values) => {
    handleCloseAtualizar();
    form.resetFields();

    values.idMembro = membro.idMembro;
    values.projeto = membro.projeto;
    values.pessoa = membro.pessoa;
    values.tipo = membro.tipo;

    if (membro.tipo === "DISCENTE") {
      values.curso = membro.curso;
    }

    if (values.modalidade == null) {
      values.modalidade = membro.modalidade;
    }

    if (values.valorBolsa == null) {
      values.valorBolsa = membro.valorBolsa;
    }

    if (values.status == null) {
      values.status = membro.status;
    }

    if (values.data_fim == null) {
      values.data_fim = membro.data_fim;
    }

    if (values.data_inicio == null) {
      values.data_inicio = membro.data_inicio;
    }

    membroService
      .updateMembros(values)
      .then(() => {
        refreshMembros();
        message.success("Cadastro Atualizado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não atualizado!");
      });
  };

  const formatarData = (date) => {
    const data = new Date(date);
    const dataFormatada = data.toLocaleDateString("pt-BR", { timeZone: "UTC" });
    return dataFormatada;
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativoConfirmar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.ModalConfirmar}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseConfirmar}
          className={styles.titulo}
        >
          <Modal.Title>Confirmar Exclusão</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <h4>
            Deseja excluir o membro <b>{membro.pessoa && membro.pessoa.nome}</b>{" "}
            agora?
          </h4>
        </Modal.Body>
        <div className={styles.confirmar}>
          <Modal.Footer>
            <Button className={styles.btnNao} onClick={handleCloseConfirmar}>
              Não
            </Button>
            <Button
              className={styles.btnSim}
              onClick={() => excluirCadastro(membro)}
            >
              Sim
            </Button>
          </Modal.Footer>
        </div>
      </Modal>

      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{membro.pessoa && membro.pessoa.nome}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Row>
            <Col md={12} className={styles.separadorMembro}>
              <Row>
                <Col md={1}>
                  <img src="/projeto.png" alt="icon" />
                </Col>
                <Col md={11}>
                  <h6 className={styles.campo}>Projeto</h6>
                  <p>{membro.projeto && membro.projeto.titulo}</p>
                </Col>
              </Row>
            </Col>
            <Col md={12} className={styles.separadorMembro}>
              <Row>
                <Col md={1}>
                  <img src="/edital.png" alt="icon" />
                </Col>
                <Col md={11}>
                  <h6 className={styles.campo}>Edital</h6>
                  <p>
                    {membro.projeto &&
                      membro.projeto.edital &&
                      membro.projeto.edital.numero +
                        "/" +
                        membro.projeto.edital.ano +
                        " - " +
                        membro.projeto.edital.descricao}
                  </p>
                </Col>
              </Row>
            </Col>

            <Col md={3} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Tipo</h5>
                </div>
                <div className={styles.cardMembro}>
                  <div class="card-body">
                    <p class="card-text text-success">{membro.tipo}</p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={3} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Modalidade</h5>
                </div>
                <div className={styles.cardMembro}>
                  <div class="card-body">
                    <p class="card-text text-success">{membro.modalidade}</p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={3} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Curso</h5>
                </div>
                <div className={styles.cardMembro}>
                  <div class="card-body">
                    <p class="card-text text-success">
                      {" "}
                      {(membro.curso && membro.curso.nome) === null
                        ? "N/A"
                        : membro.curso && membro.curso.nome}
                    </p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={3} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Valor da Bolsa</h5>
                </div>
                <div className={styles.cardMembro}>
                  <div class="card-body">
                    <p class="card-text text-success">
                      {membro.valorBolsa === null
                        ? "N/A"
                        : "R$  " + membro.valorBolsa}
                    </p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={4} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Início no Projeto</h5>
                </div>
                <div class="card-body">
                  <p class="card-text text-success">
                    {" "}
                    {formatarData(membro.data_inicio)}
                  </p>
                </div>
              </div>
            </Col>
            <Col md={4} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Finalização</h5>
                </div>
                <div class="card-body">
                  <p class="card-text text-success">
                    {membro.data_fim === null
                      ? "N/A"
                      : formatarData(membro.data_inicio)}
                  </p>
                </div>
              </div>
            </Col>
            <Col md={4} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Status</h5>
                </div>

                <div class="card-body">
                  <h6
                    className={`${
                      membro.status === "ATIVO" ? "text-success" : "text-danger"
                    }`}
                  >
                    {membro.status}
                  </h6>
                </div>
              </div>
            </Col>
            <div className={styles.atualizar}>
              <Button
                class="btn btn-success"
                md={4}
                onClick={() => chamarPaginaAtualizar(membro)}
              >
                Atualizar Dados
              </Button>
            </div>
          </Row>
        </Modal.Body>
      </Modal>

      <Modal
        id="modCome"
        show={ativoAtualizar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseAtualizar}
          className={styles.titulo}
        >
          <Modal.Title>
            {membro.pessoa && membro.pessoa.nome} - Atualizar Dados
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
            <Row>
              <Col md={12} className={styles.separadorMembroAtualizar}>
                <Row>
                  <Col md={1}>
                    <img src="/projeto.png" alt="icon" />
                  </Col>
                  <Col md={11}>
                    <h6 className={styles.campo}>Projeto: </h6>
                    <p>{membro.projeto && membro.projeto.titulo}</p>
                  </Col>
                </Row>
              </Col>

              <Col md={7} className={styles.separadorMembroAtualizar1}>
                <Row>
                  <Col md={1}>
                    <img src="/pessoa.png" alt="icon" />
                  </Col>
                  <Col md={11}>
                    <span>{membro.tipo}</span>
                    <p>{membro.pessoa && membro.pessoa.nome}</p>
                  </Col>
                </Row>
              </Col>
              <Col md={5} className={styles.separadorMembroAtualizar2}>
                <Row>
                  <Col md={2}>
                    <img src="/curso.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Curso:</h6>
                    <p>
                      {(membro.curso && membro.curso.nome) === null
                        ? "N/A"
                        : membro.curso && membro.curso.nome}
                    </p>
                  </Col>
                </Row>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item label="Modalidade:" name="modalidade">
                  <select>
                    <option value="" disabled selected>
                      {membro.modalidade}
                    </option>

                    <option value="BOLSISTA">Bolsista</option>
                    <option value="VOLUNTÁRIO">Voluntário(a)</option>
                    <option value="ORIENTADOR">Orientador(a)</option>
                  </select>
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item label="Valor da bolsa:" name="valorBolsa">
                  <Input
                    placeholder={
                      membro.valorBolsa === null
                        ? "N/A"
                        : "R$  " + membro.valorBolsa
                    }
                    type="number"
                  />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item label="Status:" name="status">
                  <select>
                    <option value="" disabled selected>
                      {membro.status}
                    </option>
                    <option value="ATIVO">Ativo</option>
                    <option value="INATIVO">Inativo</option>
                  </select>
                </Form.Item>
              </Col>
              <Col md={6} className={styles.field}>
                <Form.Item label="Início no projeto:" name="data_inicio">
                  <p>{formatarData(membro.data_inicio)}</p>
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item label="Finalização no projeto:" name="data_fim">
                  <p>{formatarData(membro.data_fim)}</p>
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={11}>
                <button className={styles.buttonSubmit} type="submit">
                  Salvar
                </button>
              </Col>
              <Col md={1}>
                <Button
                  className={styles.buttonExcluir}
                  onClick={() => confirmar(membro)}
                  type="submit"
                >
                  <img src="/excluir.png" alt="excluir" />
                </Button>
              </Col>
            </Row>
          </Form>
        </Modal.Body>
      </Modal>

      <Header />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_membrosADM">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaMembros setMembros={setMembros} />
        <Row>
          <div className={styles.membros}>
            {membros.map((membro) => {
              return (
                <Col md={6} onClick={() => chamarPagina(membro)}>
                  <ListItem
                    key={membro.idMembro}
                    nome={membro.pessoa && membro.pessoa.nome}
                    tipo={membro.tipo}
                    modalidade={membro.modalidade}
                    projeto={membro.projeto && membro.projeto.titulo}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
