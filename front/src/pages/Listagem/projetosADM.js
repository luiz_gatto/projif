import "antd/dist/antd.css";
import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemProjetosADM";
import projetoService from "../../services/projetoService";
import membroService from "../../services/membroService";
import comentarioService from "../../services/comentarioService";
import AnexoService from "../../services/anexoService";
import PesquisaProjetos from "../../Components/PesquisaProjetos";
import { saveAs } from "file-saver";

import { Modal } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import { Header } from "../../Components/HeaderADM";
import { Form, Button, message, Input } from "antd";

export default function Projetos() {
  const [ativo, setAtivo] = useState(false);
  const [ativoAtualizar, setAtivoAtualizar] = useState(false);
  const handleClose = () => setAtivo(false);
  const handleCloseAtualizar = () => setAtivoAtualizar(false);
  const [ativoConfirmar, setAtivoConfirmar] = useState(false);
  const handleCloseConfirmar = () => setAtivoConfirmar(false);

  const [projetos, setProjetos] = useState([]);
  const [projeto, setProjeto] = useState([]);
  const [membros, setMembros] = useState([]);
  const [comentarios, setComentarios] = useState([]);

  const [form] = Form.useForm();
  const [formComentario] = Form.useForm();

  useEffect(() => {
    refreshProjetos();
    return () => {};
  }, []);

  const onFinishUp = (values) => {
    handleCloseAtualizar();
    form.resetFields();

    values.edital = projeto.edital;
    values.areaConhecimento = projeto.areaConhecimento;
    values.idProjeto = projeto.idProjeto;

    if (values.titulo == null) {
      values.titulo = projeto.titulo;
    }

    if (values.data_inicio == null) {
      values.data_inicio = projeto.data_inicio;
    }

    if (values.data_fim == null) {
      values.data_fim = projeto.data_fim;
    }

    if (values.status == null) {
      values.status = projeto.status;
    }

    if (values.resumo == null) {
      values.resumo = projeto.resumo;
    }

    if (values.palavrasChave == null) {
      values.palavrasChave = projeto.palavrasChave;
    }

    if (values.acesso == null) {
      values.acesso = projeto.acesso;
    }

    if (values.tipo == null) {
      values.tipo = projeto.tipo;
    }

    if (values.anexo == null) {
      values.anexo = projeto.anexo;
    }

    projetoService
      .updateProjetos(values)
      .then(() => {
        refreshProjetos();
        message.success("Cadastro Atualizado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não atualizado!");
      });
  };

  async function refreshProjetos() {
    projetoService.retrieveAllProjetos().then((response) => {
      setProjetos(response);
    });
    membroService.retrieveAllMembros().then((response) => {
      setMembros(response);
    });
    comentarioService.retrieveAllComentarios().then((response) => {
      setComentarios(response);
    });
  }

  function chamarPagina(projeto) {
    setAtivo(true);
    setProjeto(projeto);
  }

  function chamarPaginaAtualizar(pessoa) {
    handleClose();
    setAtivoAtualizar(true);
    setProjeto(pessoa);
  }

  function confirmar(projeto) {
    handleCloseAtualizar();
    setAtivoConfirmar(true);
    setProjeto(projeto);
  }

  const excluirCadastro = (values) => {
    handleCloseConfirmar();
    projetoService
      .deleteProjeto(values.idProjeto)
      .then(() => {
        refreshProjetos();
        message.success("Cadastro apagado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não excluído!");
      });
  };

  const formatarData = (date) => {
    const data = new Date(date);
    const dataFormatada = data.toLocaleDateString("pt-BR", { timeZone: "UTC" });
    return dataFormatada;
  };

  const onFinish = (values) => {
    formComentario.resetFields();

    values.projeto = projeto;
    values.status = "PUBLICADO";

    comentarioService
      .saveComentarios(values)
      .then(() => {
        refreshProjetos();
        message.success("Comentário publicado com sucesso!");
      })
      .catch((error) => {
        message.error("Comentário não publicado!");
      });
  };

  const onFinishFailed = (erroInfo) => {
    console.log("Failed:", erroInfo);
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativoConfirmar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.ModalConfirmar}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseConfirmar}
          className={styles.titulo}
        >
          <Modal.Title>Confirmar Exclusão</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <h4>
            Certeza que deseja excluir o projeto <b>{projeto.titulo}</b> agora?
          </h4>

          <br />
          <h6 class="text-center">
            Todos os comentários e membros relacionados serão excluídos
            imediatamente.
          </h6>
        </Modal.Body>
        <div className={styles.confirmar}>
          <Modal.Footer>
            <Button className={styles.btnNao} onClick={handleCloseConfirmar}>
              Não
            </Button>

            <Button
              className={styles.btnSim}
              onClick={() => excluirCadastro(projeto)}
            >
              Sim
            </Button>
          </Modal.Footer>
        </div>
      </Modal>

      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{projeto.titulo}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-justify">
          <Row className={styles.separador}>
            <Col md={12}>
              <h6 className={styles.campo}>Edital: </h6>
              <p>
                {projeto.edital &&
                  projeto.edital.numero +
                    "/" +
                    projeto.edital.ano +
                    " - " +
                    projeto.edital.descricao}
              </p>
            </Col>
          </Row>
          <Row className={styles.separador}>
            <Col md={9}>
              <h6 className={styles.campo}>Área do Conhecimento: </h6>
              <p>
                {projeto.areaConhecimento &&
                  projeto.areaConhecimento.codArea +
                    " - " +
                    projeto.areaConhecimento.area}
              </p>
            </Col>
            <Col md={3}>
              <h6 className={styles.campo}>Status:</h6>
              <p>{projeto.status}</p>
            </Col>
          </Row>
          <Row className={styles.separador}>
            <Col md={3}>
              <h6 className={styles.campo}>Início do Projeto: </h6>
              <p>{formatarData(projeto.data_inicio)}</p>
            </Col>
            <Col md={3}>
              <h6 className={styles.campo}>Final do Projeto: </h6>
              <p>{formatarData(projeto.data_fim)}</p>
            </Col>
            <Col md={3}>
              <h6 className={styles.campo}>Acesso: </h6>
              <p>{projeto.acesso}</p>
            </Col>
            <Col md={3}>
              <h6 className={styles.campo}>Tipo:</h6>
              <p> {projeto.tipo}</p>
            </Col>
            <Col md={3} className={styles.download}>
              <h6 className={styles.campo}>Anexos:</h6>
              {projeto.anexos && projeto.anexos.length > 0
                ? projeto.anexos.map((anexo) => {
                    return (
                      <>
                        <span>{anexo.nome}</span>
                        <span
                          onClick={() => {
                            AnexoService.download(anexo).then((response) => {
                              saveAs(response, anexo.nome);
                            });
                          }}
                        >
                          <img src="/download.png" alt="icon" />
                        </span>
                      </>
                    );
                  })
                : null}
            </Col>
          </Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Resumo </h5>
              <p>{projeto.resumo}</p>
            </Col>
            <div className={styles.atualizar}>
              <Button
                class="btn btn-success"
                md={4}
                onClick={() => chamarPaginaAtualizar(projeto)}
              >
                Atualizar Dados
              </Button>
            </div>
          </Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Membros </h5>
            </Col>
          </Row>

          <table className="table table-bordered table-hover text-center">
            <thead className="thead-light">
              <tr>
                <th scope="col">Nome</th>
                <th scope="col">Tipo</th>
                <th scope="col">Modalidade</th>
                <th scope="col">Curso</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {membros.map((membro) => (
                <tr>
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td>{membro.pessoa && membro.pessoa.nome}</td>
                  ) : (
                    ""
                  )}
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td>{membro.tipo}</td>
                  ) : (
                    ""
                  )}
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td>{membro.modalidade}</td>
                  ) : (
                    ""
                  )}
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td>
                      {" "}
                      {(membro.curso && membro.curso.nome) === null
                        ? "N/A"
                        : membro.curso && membro.curso.nome}
                    </td>
                  ) : (
                    ""
                  )}
                  {membro.projeto &&
                  membro.projeto.idProjeto === projeto.idProjeto ? (
                    <td
                      className={`${
                        membro.status === "ATIVO"
                          ? "text-success"
                          : "text-danger"
                      }`}
                    >
                      {membro.status}
                    </td>
                  ) : (
                    ""
                  )}
                </tr>
              ))}
            </tbody>
          </table>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Comentários </h5>
            </Col>
          </Row>

          <Row className={styles.comentario}>
            <div class="card">
              {comentarios.map((comentario) => (
                <Col md={12}>
                  {comentario.projeto &&
                  comentario.projeto.idProjeto === projeto.idProjeto ? (
                    <h6 class="card-header">
                      <img src="/pessoa.png" alt="icon" />
                      {comentario.nome}
                    </h6>
                  ) : (
                    ""
                  )}

                  {comentario.projeto &&
                  comentario.projeto.idProjeto === projeto.idProjeto ? (
                    <div class="card-body">
                      <p class="card-text">{comentario.conteudo}</p>
                    </div>
                  ) : (
                    ""
                  )}
                </Col>
              ))}
              <h6 class="card-header">
                <img src="/comentario.png" alt="icon" />
                Tem algo a dizer sobre esse projeto? Fique a vontade!
              </h6>
            </div>
          </Row>
          <Form
            form={formComentario}
            className={styles.formComentario}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              name="conteudo"
              rules={[
                {
                  required: true,
                  message: "Insira o seu comentário",
                },
              ]}
            >
              <textarea
                class="form-control"
                rows="3"
                maxlength="250"
                placeholder="Digite seu comentário aqui..."
                name="conteudo"
              />
            </Form.Item>

            <Row className={styles.campos}>
              <Col md={5}>
                <Form.Item
                  label="Informe seu nome:"
                  name="nome"
                  rules={[
                    {
                      required: true,
                      message: "Insira o seu nome!",
                    },
                  ]}
                >
                  <Input placeholder="Insira o seu melhor email" />
                </Form.Item>
              </Col>
              <Col md={7}>
                <Form.Item
                  label="Informe seu email:"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: "Insira o seu email!",
                    },
                  ]}
                >
                  <Input placeholder="Insira o seu email" />
                </Form.Item>
              </Col>
              <button className={styles.buttonSubmit} type="submit">
                Publicar
              </button>
            </Row>
          </Form>
        </Modal.Body>
      </Modal>

      <Modal
        id="modCome"
        show={ativoAtualizar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseAtualizar}
          className={styles.titulo}
        >
          <Modal.Title>{projeto.titulo} - Atualizar Dados</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Form form={form} className={styles.stepsForm} onFinish={onFinishUp}>
            <div className={styles.fieldsContainer}>
              <Row className={styles.fields}>
                <Col md={12} className={styles.separadorProjetoAtualizar}>
                  <Row>
                    <Col md={1}>
                      <img src="/edital.png" alt="icon" />
                    </Col>
                    <Col md={11}>
                      <h6 className={styles.campo}>Edital: </h6>
                      <p>
                        {projeto.edital &&
                          projeto.edital.numero +
                            "/" +
                            projeto.edital.ano +
                            " - " +
                            projeto.edital.descricao}
                      </p>
                    </Col>
                  </Row>
                </Col>

                <Col md={12} className={styles.separadorProjetoAtualizar}>
                  <Row>
                    <Col md={1}>
                      <img src="/area.png" alt="icon" />
                    </Col>
                    <Col md={11}>
                      <h6 className={styles.campo}>Área do Conhecimento: </h6>
                      <p>
                        {projeto.areaConhecimento &&
                          projeto.areaConhecimento.codArea +
                            " - " +
                            projeto.areaConhecimento.area}
                      </p>
                    </Col>
                  </Row>
                </Col>

                <Col md={12} className={styles.separadorProjetoAtualizar}>
                  <Row>
                    <Col md={1}>
                      <img src="/projeto.png" alt="icon" />
                    </Col>
                    <Col md={11}>
                      <h6 className={styles.campo}>Título: </h6>
                      <p>{projeto.titulo}</p>
                      <Form.Item name="titulo">
                        <Input placeholder="Atualize o título" />
                      </Form.Item>
                    </Col>
                  </Row>
                </Col>
              </Row>

              <Row className={styles.fieldsProjetos}>
                <Col md={4} className={styles.field}>
                  <Form.Item label="Início do projeto:" name="data_inicio">
                    <p>{formatarData(projeto.data_inicio)}</p>
                    <Input type="date" className={styles.campoData1} />
                  </Form.Item>
                </Col>

                <Col md={4} className={styles.field}>
                  <Form.Item label="Fim do projeto:" name="data_fim">
                    <p>{formatarData(projeto.data_fim)}</p>
                    <Input type="date" className={styles.campoData2} />
                  </Form.Item>
                </Col>

                <Col md={4} className={styles.field}>
                  <Form.Item label="Satus:" name="status">
                    <select className={styles.status}>
                      <option value="" disabled selected>
                        {projeto.status}
                      </option>
                      <option value="ANDAMENTO">Andamento</option>
                      <option value="FINALIZADO">Finalizado</option>
                    </select>
                  </Form.Item>
                </Col>

                <Col md={12} className={styles.field}>
                  <Form.Item label="Resumo:" name="resumo">
                    <textarea
                      placeholder={projeto.resumo}
                      rows="3"
                      maxlength="1700"
                      name="resumo"
                    />
                  </Form.Item>
                </Col>

                <Col md={12} className={styles.field}>
                  <Form.Item label="Palavras-chave:" name="palavrasChave">
                    <Input placeholder={projeto.palavrasChave} />
                  </Form.Item>
                </Col>

                <Col md={3} className={styles.field}>
                  <Form.Item label="Acesso:" name="acesso">
                    <select>
                      <option value="" disabled selected>
                        {projeto.acesso}
                      </option>
                      <option value="PÚBLICO">Público</option>
                      <option value="SIGILOSO">Sigiloso</option>
                    </select>
                  </Form.Item>
                </Col>

                <Col md={3} className={styles.field}>
                  <Form.Item label="Tipo:" name="tipo">
                    <select>
                      <option value="" disabled selected>
                        {projeto.tipo}
                      </option>
                      <option value="ENSINO">Ensino</option>
                      <option value="EXTENSÃO">Extensão</option>
                      <option value="PESQUISA">Pesquisa</option>
                      <option value="TCC">TCC</option>
                    </select>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col md={3} className={styles.download}>
                  <h6 className={styles.campo}>Anexos:</h6>
                  {projeto.anexos && projeto.anexos.length > 0
                    ? projeto.anexos.map((anexo) => {
                        return (
                          <>
                            <span>{anexo.nome}</span>
                            <span
                              onClick={() => {
                                AnexoService.download(anexo).then(
                                  (response) => {
                                    saveAs(response, anexo.nome);
                                  }
                                );
                              }}
                            >
                              <img src="/download.png" alt="icon" />
                            </span>
                          </>
                        );
                      })
                    : null}
                </Col>
              </Row>
              <Row>
                <Col md={11}>
                  <button className={styles.buttonSubmit} type="submit">
                    Salvar
                  </button>
                </Col>
                <Col md={1}>
                  <Button
                    className={styles.buttonExcluir}
                    onClick={() => confirmar(projeto)}
                    type="submit"
                  >
                    <img src="/excluir.png" alt="excluir" />
                  </Button>
                </Col>
              </Row>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      <Header />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_projetosADM">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaProjetos setProjetos={setProjetos} />

        <Row>
          <div className={styles.projetos}>
            {projetos.map((projeto) => {
              return (
                <Col md={12} onClick={() => chamarPagina(projeto)}>
                  <ListItem
                    className={styles.item}
                    key={projeto.idProjeto}
                    titulo={projeto.titulo}
                    status={projeto.acesso}
                    edital={
                      projeto.edital &&
                      projeto.edital.numero +
                        "/" +
                        projeto.edital.ano +
                        " - " +
                        projeto.edital.descricao
                    }
                    area={
                      projeto.areaConhecimento &&
                      projeto.areaConhecimento.codArea +
                        " - " +
                        projeto.areaConhecimento.area
                    }
                    tipo={projeto.tipo}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
