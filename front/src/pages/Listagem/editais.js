import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemEditais";
import editalService from "../../services/editalService";
import projetoService from "../../services/projetoService";
import PesquisaEdital from "../../Components/PesquisaEditais";

import { Modal } from "react-bootstrap";
import { HeaderVisitante } from "../../Components/Header";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";

export default function Editais() {
  const [ativo, setAtivo] = useState(false);
  const handleClose = () => setAtivo(false);

  const [editais, setEditais] = useState([]);
  const [edital, setEdital] = useState([]);
  const [projetos, setProjetos] = useState([]);

  useEffect(() => {
    refreshEditais();
    return () => {};
  }, []);

  async function refreshEditais() {
    editalService.retrieveAllEditais().then((response) => {
      setEditais(response);
    });
    projetoService.retrieveAllProjetos().then((response) => {
      setProjetos(response);
    });
  }

  function chamarPagina(edital) {
    setAtivo(true);
    setEdital(edital);
  }

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>
            {edital.numero + "/" + edital.ano + " - " + edital.descricao}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Row>
            <Col md={12} className={styles.separadorEdital}>
              <Row>
                <Col md={5}>
                  <img src="/orgao.png" alt="icon" />
                </Col>
                <Col md={7}>
                  <h6 className={styles.campo}>Orgão ou Instituição</h6>
                  <p>{edital.orgao}</p>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Projetos</h5>
            </Col>
          </Row>
          <table className="table table-bordered table-hover text-center">
            <thead className="thead-light">
              <tr>
                <th scope="col">Título</th>
                <th scope="col">Área do Conhecimento</th>
                <th scope="col">Tipo</th>
                <th scope="col">Acesso</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {projetos.map((projeto) => (
                <tr>
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td>{projeto.titulo}</td>
                  ) : (
                    ""
                  )}
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td>
                      {projeto.areaConhecimento &&
                        projeto.areaConhecimento.codArea +
                          " - " +
                          projeto.areaConhecimento.area}
                    </td>
                  ) : (
                    ""
                  )}
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td>{projeto.tipo}</td>
                  ) : (
                    ""
                  )}
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td> {projeto.acesso}</td>
                  ) : (
                    ""
                  )}
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td
                      className={`${
                        projeto.status === "ANDAMENTO"
                          ? "text-success"
                          : "text-danger"
                      }`}
                    >
                      {projeto.status}
                    </td>
                  ) : (
                    ""
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>
      </Modal>
      <HeaderVisitante />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_editais">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaEdital setEditais={setEditais} />
        <Row>
          <div className={styles.editais}>
            {editais.map((edital) => {
              return (
                <Col md={4} onClick={() => chamarPagina(edital)}>
                  <ListItem
                    key={edital.idEdital}
                    numero={edital.numero}
                    ano={edital.ano}
                    descricao={edital.descricao}
                    orgao={edital.orgao}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
