import "bootstrap/dist/css/bootstrap.min.css";
import ListItemADM from "../../Components/ListItemComentariosADM";
import comentarioService from "../../services/comentarioService";
import projetoService from "../../services/projetoService";
import PesquisaComentarios from "../../Components/PesquisaComentarios";
import { Form, message } from "antd";

import styles from "../Listagem/styles.module.scss";
import { Modal } from "react-bootstrap";
import { useEffect, useState } from "react";
import { Header } from "../../Components/HeaderADM";
import { Row, Col } from "react-bootstrap";

export default function Comentarios() {
  const [comentarios, setComentarios] = useState([]);
  const [comentario, setComentario] = useState([]);
  const [projetos, setProjetos] = useState([]);
  const [ativo, setAtivo] = useState(false);

  const handleClose = () => setAtivo(false);

  const [form] = Form.useForm();

  useEffect(() => {
    refresh();
    return () => {};
  });

  async function refresh() {
    comentarioService.retrieveAllComentarios().then((response) => {
      setComentarios(response);
    });

    projetoService.retrieveAllProjetos().then((response) => {
      setProjetos(response);
    });
  }

  function chamarPagina(comentario) {
    setAtivo(true);
    setComentario(comentario);
  }

  const despublicar = (values) => {
    handleClose();
    form.resetFields();

    values.idComentario = comentario.idComentario;
    values.projeto = comentario.projeto;

    if (values.nome == null) {
      values.nome = comentario.nome;
    }

    if (values.email == null) {
      values.email = comentario.email;
    }

    if (values.conteudo == null) {
      values.conteudo = comentario.conteudo;
    }

    if (values.status == null) {
      values.status = comentario.status;
    }

    comentarioService
      .updateComentario(values)
      .then(() => {
        refresh();
        message.success("Comentário despublicado com sucesso!");
      })
      .catch((error) => {
        message.error("Comentário não foi despublicado");
      });
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>
            Comentário nº {comentario.idComentario} - Alterar Visibilidade
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Form form={form} className={styles.stepsForm} onFinish={despublicar}>
            <Row>
              <Col md={6} className={styles.separadorComentario}>
                <Row>
                  <Col md={2}>
                    <img src="/pessoa.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>Nome: </h6>
                    <p>{comentario.nome}</p>
                  </Col>
                </Row>
              </Col>
              <Col md={6} className={styles.separadorComentario1}>
                <Row>
                  <Col md={2}>
                    <img src="/email.png" alt="icon" />
                  </Col>
                  <Col md={10}>
                    <h6 className={styles.campo}>E-mail: </h6>
                    <p>{comentario.email}</p>
                  </Col>
                </Row>
              </Col>
              <Col md={12} className={styles.separadorComentario1}>
                <Row>
                  <Col md={1}>
                    <img src="/projeto.png" alt="icon" />
                  </Col>
                  <Col md={11}>
                    <h6 className={styles.campo}>Projeto</h6>
                    <p>{comentario.projeto && comentario.projeto.titulo}</p>
                  </Col>
                </Row>
              </Col>
              <Col md={12} className={styles.separadorComentario1}>
                <Row>
                  <Col md={1}>
                    <img src="/iconFeed.png" alt="icon" />
                  </Col>
                  <Col md={11}>
                    <h6 className={styles.campo}>Comentário: </h6>
                    <p>{comentario.conteudo}</p>
                  </Col>
                </Row>
              </Col>
              <Col md={12} className={styles.separadorComentario1}>
                <Row>
                  <Col md={1}>
                    <img src="/status.png" alt="icon" />
                  </Col>
                  <Col md={11}>
                    <Form.Item label="Visibilidade:" name="status">
                      <select className={styles.status}>
                        <option value="" disabled selected>
                          {comentario.status}
                        </option>
                        <option value="PUBLICADO">Publicado</option>
                        <option value="OCULTO">Oculto</option>
                      </select>
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col md={12}>
                <button className={styles.buttonRedefinirSenha} type="submit">
                  Salvar
                </button>
              </Col>
            </Row>
          </Form>
        </Modal.Body>
      </Modal>

      <Header />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_comentariosADM">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaComentarios setComentarios={setComentarios} />
        <Row>
          <div className={styles.comentariosADM}>
            {comentarios.map((comentario) => {
              return (
                <Col
                  md={12}
                  onClick={() => chamarPagina(comentario)}
                  className={styles.item}
                >
                  <ListItemADM
                    key={comentario.idComentario}
                    titulo={comentario.projeto && comentario.projeto.titulo}
                    status={comentario.status}
                    nome={comentario.nome}
                    email={comentario.email}
                    conteudo={comentario.conteudo}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
