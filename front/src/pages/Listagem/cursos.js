import { HeaderVisitante } from "../../Components/Header";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";

import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemCursos";
import CursoService from "../../services/cursoService";
import PesquisaCurso from "../../Components/PesquisaCursos";
import membroService from "../../services/membroService";

import { Modal } from "react-bootstrap";

export default function Cursos() {
  const [ativo, setAtivo] = useState(false);
  const handleClose = () => setAtivo(false);

  const [cursos, setCursos] = useState([]);
  const [curso, setCurso] = useState([]);
  const [membros, setMembros] = useState([]);

  useEffect(() => {
    refreshCursos();
    return () => {};
  }, []);

  async function refreshCursos() {
    CursoService.retrieveAllCursos().then((response) => {
      setCursos(response);
    });

    membroService.retrieveAllMembros().then((response) => {
      setMembros(response);
    });
  }

  function chamarPagina(curso) {
    setAtivo(true);
    setCurso(curso);
  }

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{curso.nome}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-center">
          <Row>
            <Col md={6} className={styles.separadorCurso}>
              <Row>
                <Col md={3}>
                  <img src="/curso.png" alt="icon" />
                </Col>
                <Col md={9}>
                  <h6 className={styles.campo}>Modalidade </h6>
                  <p>{curso.modalidade}</p>
                </Col>
              </Row>
            </Col>

            <Col md={6} className={styles.separadorCurso}>
              <Row>
                <Col md={3}>
                  <img src="/campus.png" alt="icon" />
                </Col>
                <Col md={9}>
                  <h6 className={styles.campo}>Campus </h6>
                  <p>{curso.campus}</p>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Membros Relacionados </h5>
            </Col>
          </Row>
        </Modal.Body>

        <table className="table table-bordered table-hover text-center">
          <thead className="thead-light">
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Projeto</th>
              <th scope="col">Modalidade</th>
              <th scope="col">Tipo</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
            {membros.map((membro) => (
              <tr>
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td>{membro.pessoa && membro.pessoa.nome}</td>
                ) : (
                  ""
                )}
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td>{membro.projeto && membro.projeto.titulo}</td>
                ) : (
                  ""
                )}
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td>{membro.modalidade}</td>
                ) : (
                  ""
                )}
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td>{membro.tipo}</td>
                ) : (
                  ""
                )}
                {membro.curso && membro.curso.idCurso === curso.idCurso ? (
                  <td
                    className={`${
                      membro.status === "ATIVO" ? "text-success" : "text-danger"
                    }`}
                  >
                    {membro.status}
                  </td>
                ) : (
                  ""
                )}
              </tr>
            ))}
          </tbody>
        </table>
      </Modal>
      <HeaderVisitante />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_cursos">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaCurso setCursos={setCursos} />
        <Row>
          <div className={styles.cursos}>
            {cursos.map((curso) => {
              return (
                <Col md={4} onClick={() => chamarPagina(curso)}>
                  <ListItem
                    key={curso.idCurso}
                    nome={curso.nome}
                    modalidade={curso.modalidade}
                    campus={curso.campus}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
