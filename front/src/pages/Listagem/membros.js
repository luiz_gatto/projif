import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemMembros";
import membroService from "../../services/membroService";
import pessoaService from "../../services/pessoaService";
import PesquisaMembros from "../../Components/PesquisaMembros";

import { Modal } from "react-bootstrap";
import { HeaderVisitante } from "../../Components/Header";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";

export default function Membros() {
  const [ativo, setAtivo] = useState(false);
  const handleClose = () => setAtivo(false);

  const [membro, setMembro] = useState([]);
  const [membros, setMembros] = useState([]);
  const [pessoas, setPessoas] = useState([]);

  useEffect(() => {
    refreshMembros();
  }, []);

  async function refreshMembros() {
    membroService.retrieveAllMembros().then((response) => {
      setMembros(response);
    });
    pessoaService.retrieveAllPessoas().then((response) => {
      setPessoas(response);
    });
  }

  function chamarPagina(membro) {
    setAtivo(true);
    setMembro(membro);
  }

  const formatarData = (date) => {
    const data = new Date(date);
    const dataFormatada = data.toLocaleDateString("pt-BR", { timeZone: "UTC" });
    return dataFormatada;
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>{membro.pessoa && membro.pessoa.nome}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Row>
            <Col md={12} className={styles.separadorMembro}>
              <Row>
                <Col md={1}>
                  <img src="/projeto.png" alt="icon" />
                </Col>
                <Col md={11}>
                  <h6 className={styles.campo}>Projeto</h6>
                  <p>{membro.projeto && membro.projeto.titulo}</p>
                </Col>
              </Row>
            </Col>
            <Col md={12} className={styles.separadorMembro}>
              <Row>
                <Col md={1}>
                  <img src="/edital.png" alt="icon" />
                </Col>
                <Col md={11}>
                  <h6 className={styles.campo}>Edital</h6>
                  <p>
                    {membro.projeto &&
                      membro.projeto.edital &&
                      membro.projeto.edital.numero +
                        "/" +
                        membro.projeto.edital.ano +
                        " - " +
                        membro.projeto.edital.descricao}
                  </p>
                </Col>
              </Row>
            </Col>

            <Col md={3} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Tipo</h5>
                </div>
                <div className={styles.cardMembro}>
                  <div class="card-body">
                    <p class="card-text text-success">{membro.tipo}</p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={3} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Modalidade</h5>
                </div>
                <div className={styles.cardMembro}>
                  <div class="card-body">
                    <p class="card-text text-success">{membro.modalidade}</p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={3} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Curso</h5>
                </div>
                <div className={styles.cardMembro}>
                  <div class="card-body">
                    <p class="card-text text-success">
                      {" "}
                      {(membro.curso && membro.curso.nome) === null
                        ? "N/A"
                        : membro.curso && membro.curso.nome}
                    </p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={3} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Valor da Bolsa</h5>
                </div>
                <div className={styles.cardMembro}>
                  <div class="card-body">
                    <p class="card-text text-success">
                      {membro.valorBolsa === null
                        ? "N/A"
                        : "R$  " + membro.valorBolsa}
                    </p>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={4} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Início no Projeto</h5>
                </div>

                <div class="card-body">
                  <p class="card-text text-success">
                    {" "}
                    {formatarData(membro.data_inicio)}
                  </p>
                </div>
              </div>
            </Col>
            <Col md={4} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Finalização</h5>
                </div>

                <div class="card-body">
                  <p class="card-text text-success">
                    {membro.data_fim === null
                      ? "N/A"
                      : formatarData(membro.data_inicio)}
                  </p>
                </div>
              </div>
            </Col>
            <Col md={4} className={styles.cardMembroItem}>
              <div class="card border-success mb-3 text-center">
                <div class="card-header">
                  <h5>Status</h5>
                </div>

                <div class="card-body">
                  <h6
                    className={`${
                      membro.status === "ATIVO" ? "text-success" : "text-danger"
                    }`}
                  >
                    {membro.status}
                  </h6>
                </div>
              </div>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>

      <HeaderVisitante />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_membros">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaMembros setMembros={setMembros} />
        <Row>
          <div className={styles.membros}>
            {membros.map((membro) => {
              return (
                <Col md={6} onClick={() => chamarPagina(membro)}>
                  <ListItem
                    key={membro.idMembro}
                    nome={membro.pessoa && membro.pessoa.nome}
                    tipo={membro.tipo}
                    modalidade={membro.modalidade}
                    projeto={membro.projeto && membro.projeto.titulo}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
