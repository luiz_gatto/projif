import styles from "../Listagem/styles.module.scss";
import ListItem from "../../Components/ListItemEditais";
import editalService from "../../services/editalService";
import projetoService from "../../services/projetoService";
import PesquisaEdital from "../../Components/PesquisaEditais";
import EditalService from "../../services/editalService";

import { Modal } from "react-bootstrap";
import { Form, Input, message, Button } from "antd";
import { Header } from "../../Components/HeaderADM";
import { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";

export default function Editais() {
  const [ativo, setAtivo] = useState(false);
  const [ativoAtualizar, setAtivoAtualizar] = useState(false);
  const handleClose = () => setAtivo(false);
  const handleCloseAtualizar = () => setAtivoAtualizar(false);
  const [ativoConfirmar, setAtivoConfirmar] = useState(false);
  const handleCloseConfirmar = () => setAtivoConfirmar(false);

  const [editais, setEditais] = useState([]);
  const [edital, setEdital] = useState([]);
  const [projetos, setProjetos] = useState([]);

  const [form] = Form.useForm();

  useEffect(() => {
    refreshEditais();
    return () => {};
  }, []);

  async function refreshEditais() {
    editalService.retrieveAllEditais().then((response) => {
      setEditais(response);
    });

    projetoService.retrieveAllProjetos().then((response) => {
      setProjetos(response);
    });
  }

  const onFinish = (values) => {
    handleCloseAtualizar();
    form.resetFields();

    values.idEdital = edital.idEdital;

    if (values.numero == null) {
      values.numero = edital.numero;
    }
    if (values.ano == null) {
      values.ano = edital.ano;
    }
    if (values.orgao == null) {
      values.orgao = edital.orgao;
    }
    if (values.descricao == null) {
      values.descricao = edital.descricao;
    }

    EditalService.updateEditais(values)
      .then(() => {
        refreshEditais();
        message.success("Cadastro Atualizado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não atualizado!");
      });
  };

  function chamarPagina(edital) {
    setAtivo(true);
    setEdital(edital);
  }

  function chamarPaginaAtualizar(edital) {
    handleClose();
    setAtivoAtualizar(true);
    setEdital(edital);
  }

  function confirmar(edital) {
    handleCloseAtualizar();
    setAtivoConfirmar(true);
    setEdital(edital);
  }

  const excluirCadastro = (values) => {
    handleCloseConfirmar();
    editalService
      .deleteEdital(values.idEdital)
      .then(() => {
        refreshEditais();
        message.success("Cadastro apagado com sucesso!");
      })
      .catch((error) => {
        message.error("Cadastro não excluído!");
      });
  };

  return (
    <div className={styles.wrapper}>
      <Modal
        id="modCome"
        show={ativoConfirmar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.ModalConfirmar}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseConfirmar}
          className={styles.titulo}
        >
          <Modal.Title>Confirmar Exclusão</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <h4>
            Certeza que deseja excluir o edital{" "}
            <b>{edital.numero + "/" + edital.ano + " - " + edital.descricao}</b>{" "}
            agora?
          </h4>
          <br />
          <h6 class="text-center">
            Todos os projetos relacionados serão excluídos imediatamente.
          </h6>
        </Modal.Body>
        <div className={styles.confirmar}>
          <Modal.Footer>
            <Button className={styles.btnNao} onClick={handleCloseConfirmar}>
              Não
            </Button>

            <Button
              className={styles.btnSim}
              onClick={() => excluirCadastro(edital)}
            >
              Sim
            </Button>
          </Modal.Footer>
        </div>
      </Modal>

      <Modal
        id="modCome"
        show={ativo}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleClose}
          className={styles.titulo}
        >
          <Modal.Title>
            {edital.numero + "/" + edital.ano + " - " + edital.descricao}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Row>
            <Col md={12} className={styles.separadorEdital}>
              <Row>
                <Col md={5}>
                  <img src="/orgao.png" alt="icon" />
                </Col>
                <Col md={7}>
                  <h6 className={styles.campo}>Orgão ou Instituição</h6>
                  <p>{edital.orgao}</p>
                </Col>
              </Row>
            </Col>
            <div className={styles.atualizar}>
              <Button
                class="btn btn-success"
                md={4}
                onClick={() => chamarPaginaAtualizar(edital)}
              >
                Atualizar Dados
              </Button>
            </div>
          </Row>
          <Row className={styles.separadorTitulo}>
            <Col md={12}>
              <h5>Projetos</h5>
            </Col>
          </Row>
          <table className="table table-bordered table-hover text-center">
            <thead className="thead-light">
              <tr>
                <th scope="col">Título</th>
                <th scope="col">Área do Conhecimento</th>
                <th scope="col">Tipo</th>
                <th scope="col">Acesso</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {projetos.map((projeto) => (
                <tr>
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td>{projeto.edital && projeto.titulo}</td>
                  ) : (
                    ""
                  )}
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td>
                      {projeto.edital &&
                        projeto.areaConhecimento &&
                        projeto.areaConhecimento.codArea +
                          " - " +
                          projeto.areaConhecimento.area}
                    </td>
                  ) : (
                    ""
                  )}
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td>{projeto.tipo}</td>
                  ) : (
                    ""
                  )}
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td> {projeto.acesso}</td>
                  ) : (
                    ""
                  )}
                  {projeto.edital &&
                  projeto.edital.idEdital === edital.idEdital ? (
                    <td
                      className={`${
                        projeto.status === "ANDAMENTO"
                          ? "text-success"
                          : "text-danger"
                      }`}
                    >
                      {projeto.status}
                    </td>
                  ) : (
                    ""
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>
      </Modal>

      <Modal
        id="modCome"
        show={ativoAtualizar}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        dialogClassName={styles.Modal}
        centered
      >
        <Modal.Header
          closeButton
          onClick={handleCloseAtualizar}
          className={styles.titulo}
        >
          <Modal.Title>
            {edital.numero + "/" + edital.ano + " - " + edital.descricao} -
            Atualizar Dados
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-left">
          <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
            <div className={styles.fieldsContainer}>
              <Row className={styles.fields}>
                <Col md={4} className={styles.field}>
                  <Form.Item label="Número do edital:" name="numero">
                    <Input placeholder={edital.numero} />
                  </Form.Item>
                </Col>

                <Col md={4} className={styles.field}>
                  <Form.Item label="Ano do edital:" name="ano">
                    <Input placeholder={edital.ano} />
                  </Form.Item>
                </Col>

                <Col md={4} className={styles.field}>
                  <Form.Item label="Orgão:" name="orgao">
                    <Input placeholder={edital.orgao} />
                  </Form.Item>
                </Col>

                <Col md={12} className={styles.field}>
                  <Form.Item label="Descrição:" name="descricao">
                    <Input placeholder={edital.descricao} />
                  </Form.Item>
                </Col>

                <Col md={11}>
                  <button className={styles.buttonSubmit} type="submit">
                    Salvar
                  </button>
                </Col>
                <Col md={1}>
                  <Button
                    className={styles.buttonExcluir}
                    onClick={() => confirmar(edital)}
                    type="submit"
                  >
                    <img src="/excluir.png" alt="excluir" />
                  </Button>
                </Col>
              </Row>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      <Header />
      <main className={styles.ListagemContainer}>
        <header className={styles.topo}>
          <a href="/#/listagem_editaisADM">
            <img src="/logo.png" alt="icon" />
          </a>
        </header>
        <PesquisaEdital setEditais={setEditais} />
        <Row>
          <div className={styles.editais}>
            {editais.map((edital) => {
              return (
                <Col md={4} onClick={() => chamarPagina(edital)}>
                  <ListItem
                    key={edital.idEdital}
                    numero={edital.numero}
                    ano={edital.ano}
                    descricao={edital.descricao}
                    orgao={edital.orgao}
                  />
                </Col>
              );
            })}
          </div>
        </Row>
      </main>
    </div>
  );
}
