import styles from '../Editais/styles.module.scss';
import PessoaService from '../../services/pessoaService';
import InputMask from "react-input-mask";
import { Form, Input, message } from 'antd';
import { Row, Col } from 'react-bootstrap';
import { Header } from '../../Components/HeaderADM';

const InputTel = (props) => (
  <InputMask mask="(99)99999-9999"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o número de telefone" />
);

const InputCPF = (props) => (
  <InputMask mask="999.999.999-99"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o CPF" />
);

const InputCEP = (props) => (
  <InputMask mask="99999-999"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o CEP" />
);


export function Pessoas(props) {
  const [form] = Form.useForm();

  const onFinish = (values) => {
    
    form.resetFields();

    PessoaService.savePessoas(values).then(() => {
      message.success("Pessoa incluída com sucesso!")
    })
      .catch((error) => {
        message.error("Cadastro não foi concluído!")
      });
  }

  return (
    <div className={styles.wrapper}>

      <Header />

      <main className={styles.App}>
        <h1>Cadastrar Pessoas</h1>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish}>

          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Nome:"
                  name="nome"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o nome completo',
                    },
                  ]}>
                  <Input placeholder="Insira o nome completo" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="CPF:"
                  name="cpf"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o CPF',
                    },
                  ]}>
                  <InputCPF />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Data de Nascimento: "
                  name="data_nascimento"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a data de nascimento',
                    },
                  ]}>
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Currículo Lattes:"
                  name="lattes"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o link do currículo lattes',
                    },
                  ]}>
                  <Input placeholder="Insira o link do currículo lattes" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Email:"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o endereço de email',
                    },
                  ]}>
                  <Input type="email" placeholder="Insira o endereço de email" />
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Telefone:"
                  name="telefone"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o número de telefone',
                    },
                  ]}>
                  <InputTel />
                </Form.Item>
              </Col>

              <Col md={10} className={styles.field}>
                <Form.Item
                  label="Logradouro:"
                  name="logradouro"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o endereço!',
                    },

                  ]}>
                  <Input placeholder="Insira o endereço" />
                </Form.Item>
              </Col>
              <Col md={2} className={styles.field}>
                <Form.Item
                  label="Nº:"
                  name="numero"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o número!',
                    },

                  ]}>
                  <Input placeholder="Insira o número" />
                </Form.Item>
              </Col>
              <Col md={3} className={styles.field}>
                <Form.Item
                  label="Bairro:"
                  name="bairro"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o bairro!',
                    },

                  ]}>
                  <Input placeholder="Insira o bairro" />
                </Form.Item>
              </Col>
              <Col md={3} className={styles.field}>
                <Form.Item
                  label="Cidade:"
                  name="cidade"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a cidade!',
                    },

                  ]}>
                  <Input placeholder="Insira a cidade" />
                </Form.Item>
              </Col>
              <Col md={2} className={styles.field}>
                <Form.Item
                  label="CEP:"
                  name="cep"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o CEP!',
                    },

                  ]}>
                  <InputCEP/>
                </Form.Item>
              </Col>
              <Col md={3} className={styles.field}>
                <Form.Item
                  label="Estado:"
                  name="estado"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o Estado!',
                    },

                  ]}>
                  <Input placeholder="Insira o Estado" />
                </Form.Item>
              </Col>
              <Col md={1} className={styles.field}>
                <Form.Item
                  label="UF:"
                  name="uf"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a UF!',
                    },

                  ]}>
                  <Input maxLength="2"/>
                </Form.Item>
              </Col>
          
              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit" >
                  Salvar
                </button>
              </Col>
            </Row>
          </div>
        </Form>
      </main>
    </div>

  );
}