import styles from "../Login/styles.module.scss";

import { Row, Col } from "react-bootstrap";
import { Form, Input, message } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { useState } from "react";
import { Modal } from "react-bootstrap";
import { Button } from "antd";
import LoginService from "../../services/loginService";
import { ACCESS_TOKEN } from "../../services/constantes";

export default function Login({ setUsuarioLogado }) {
  const [ativo, setAtivo] = useState(false);
  const [email, setEmail] = useState("");
  const handleClose = () => setAtivo(false);

  function chamarPagina() {
    if (
      !email ||
      email.length === 0 ||
      !email.includes("@") ||
      !email.includes(".")
    ) {
      message.warning("Preencha o campo de email");
      return;
    }
    LoginService.recuperarSenha(email)
      .then((resp) => {
        setAtivo(true);
      })
      .catch((err) => {
        if (err?.response?.data?.message?.includes("No value")) {
          message.error("Email não cadastrado!");
          return;
        }
        message.error("Erro ao enviar email de redefinição, tente novamente!");
      });
  }
  const onFinish = (values) => {
    LoginService.login({ login: values.email, senha: values.senha })
      .then((resp) => {
        console.log(resp);
        localStorage.setItem(ACCESS_TOKEN, resp.accessToken);
        // setUsuarioLogado(true);
        message.success("Login realizado com sucesso!");
        window.location.href = "/#/dashboard";
      })
      .catch((error) => {
        message.error("Usuario ou senha inválidos");
      });
  };

  return (
    <div className={styles.topo}>
      <img src="/logo-branca.png" alt="icon" />
      <main className={styles.App}>
        <Modal
          id="modCome"
          show={ativo}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          dialogClassName={styles.ModalConfirmar}
          centered
        >
          <Modal.Header
            closeButton
            onClick={handleClose}
            className={styles.titulo}
          >
            <Modal.Title>Redefinir Senha</Modal.Title>
          </Modal.Header>
          <Modal.Body className="text-center">
            <h4>Uma mensagem foi enviada para o email cadastrado!</h4>
            <h5>Siga os passos para redefinir sua senha!</h5>
          </Modal.Body>
        </Modal>
        <Form className={styles.stepsForm} onFinish={onFinish}>
          <h1> Login </h1>
          <div className={styles.fieldsContainer}>
            <Row className={styles.fields}>
              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Email:"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: "Insira o email",
                    },
                  ]}
                >
                  <Input
                    placeholder="Insira o email"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Item>
              </Col>

              <Col md={12} className={styles.field}>
                <Form.Item
                  label="Senha:"
                  name="senha"
                  rules={[
                    {
                      required: true,
                      message: "Insira a senha",
                    },
                  ]}
                >
                  <Input.Password
                    placeholder="Insira a senha"
                    iconRender={(visible) =>
                      visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                    }
                  />
                </Form.Item>
              </Col>

              <button className={styles.buttonSubmit} type="submit">
                Entrar
              </button>
            </Row>
          </div>
        </Form>
        <Button className={styles.btnSim} onClick={chamarPagina}>
          Esqueci minha senha
        </Button>
      </main>
    </div>
  );
}
