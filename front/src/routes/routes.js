import React, { useState, useEffect } from "react";
import { Router, Switch, Route } from "react-router-dom";
import Home from "../pages/Home";
import HomeADM from "../pages/HomeADM/index";
import Projetos from "../pages/Projetos";
import Membros from "../pages/Membros";
import Login from "../pages/Login";
import ListaEditais from "../pages/Listagem/editais";
import ListaProjetos from "../pages/Listagem/projetos";
import ListaPessoas from "../pages/Listagem/pessoas";
import ListaCursos from "../pages/Listagem/cursos";
import ListaMembros from "../pages/Listagem/membros";
import ListaUsuarios from "../pages/Listagem/usuarios";
import ListaComentarios from "../pages/Listagem/comentarios";
import ListaEditaisADM from "../pages/Listagem/editaisADM";
import ListaProjetosADM from "../pages/Listagem/projetosADM";
import ListaPessoasADM from "../pages/Listagem/pessoasADM";
import ListaCursosADM from "../pages/Listagem/cursosADM";
import { Cursos } from "../pages/Cursos";
import RedefinirSenha from "../pages/RedefinirSenha";
import ListaMembrosADM from "../pages/Listagem/membrosADM";
import ListaComentariosADM from "../pages/Listagem/comentariosADM";
import ListaAreas from "../pages/Listagem/areas";
import ListaAreasADM from "../pages/Listagem/areasADM";
import Usuarios from "../pages/Usuarios";
import Dashboard from "../pages/Dashboard";
import { Editais } from "../pages/Editais";
import { Pessoas } from "../pages/Pessoas";
import PrivateRoute from "./PrivateRoute";
import { ACCESS_TOKEN } from "../services/constantes";
import { createHashHistory } from "history";
export const history = createHashHistory();

export default function Routes() {
  const [usuarioLogado, setUsuarioLogado] = useState(false);

  useEffect(() => {
    setUsuarioLogado(!!localStorage.getItem(ACCESS_TOKEN));
  }, []);

  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/home" component={Home} />
        <PrivateRoute exact path="/homeADM" component={HomeADM} />
        <PrivateRoute exact path="/projetos" component={Projetos} />
        <PrivateRoute exact path="/membros" component={Membros} />
        <PrivateRoute exact path="/usuarios" component={Usuarios} />
        <PrivateRoute exact path="/dashboard" component={Dashboard} />
        <PrivateRoute exact path="/cursos" component={Cursos} />
        <PrivateRoute exact path="/editais" component={Editais} />
        <PrivateRoute exact path="/pessoas" component={Pessoas} />
        <Route exact path="/listagem_pessoas" component={ListaPessoas} />
        <Route exact path="/listagem_editais" component={ListaEditais} />
        <PrivateRoute
          exact
          path="/listagem_usuarios"
          component={ListaUsuarios}
        />
        <Route exact path="/listagem_membros" component={ListaMembros} />
        <Route exact path="/listagem_cursos" component={ListaCursos} />
        <Route
          exact
          path="/listagem_comentarios"
          component={ListaComentarios}
        />
        <Route exact path="/listagem_projetos" component={ListaProjetos} />
        <Route exact path="/listagem_areas" component={ListaAreas} />
        <PrivateRoute
          exact
          path="/listagem_projetosADM"
          component={ListaProjetosADM}
        />
        <PrivateRoute
          exact
          path="/listagem_pessoasADM"
          component={ListaPessoasADM}
        />
        <PrivateRoute
          exact
          path="/listagem_editaisADM"
          component={ListaEditaisADM}
        />
        <PrivateRoute
          exact
          path="/listagem_membrosADM"
          component={ListaMembrosADM}
        />
        <PrivateRoute
          exact
          path="/listagem_cursosADM"
          component={ListaCursosADM}
        />
        <PrivateRoute
          exact
          path="/listagem_areasADM"
          component={ListaAreasADM}
        />
        <PrivateRoute
          exact
          path="/listagem_comentariosADM"
          component={ListaComentariosADM}
        />

        <Route
          exact
          path="/login"
          component={Login}
          setUsuarioLogado={setUsuarioLogado}
        />
        <Route
          path="/redefinirSenha/:hash"
          exact
          render={(props) => <RedefinirSenha {...props} />}
        />
      </Switch>
    </Router>
  );
}
