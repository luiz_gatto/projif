package br.com.yanivilasboas.projif.resource;

import br.com.yanivilasboas.projif.domain.Dashboard;
import br.com.yanivilasboas.projif.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dashboard")
@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
public class DashboardResource {

    private final ProjetoRepository projetoRepository;
    private final EditalRepository editalRepository;
    private final MembroRepository membroRepository;
    private final CursoRepository cursoRepository;
    private final ComentarioRepository comentarioRepository;
    private final UsuarioRepository usuarioRepository;


    @PostMapping
    public ResponseEntity<Dashboard> filtro() {

        Dashboard dashboard = new Dashboard();
        dashboard.setQtdProjeto(projetoRepository.count());
        dashboard.setQtdEdital(editalRepository.count());
        dashboard.setQtdMembros(membroRepository.count());
        dashboard.setQtdCursos(cursoRepository.count());
        dashboard.setQtdComentarios(comentarioRepository.count());
        dashboard.setQtdUsuarios(usuarioRepository.count());

        return ResponseEntity.ok(dashboard);
    }
}
