package br.com.yanivilasboas.projif.domain;

public enum StatusComentario {
	PUBLICADO,
	OCULTO
}
