package br.com.yanivilasboas.projif.domain;

import lombok.Data;

@Data
public class Dashboard {

    private Long qtdProjeto;
    private Long qtdEdital;
    private Long qtdMembros;
    private Long qtdCursos;
    private Long qtdComentarios;
    private Long qtdUsuarios;


}
