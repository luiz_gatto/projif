package br.com.yanivilasboas.projif.domain;

public enum TipoAcessoProjeto {

	PÚBLICO,
	SIGILOSO
}
