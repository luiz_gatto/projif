package br.com.yanivilasboas.projif.domain;

public enum StatusProjeto {
	ANDAMENTO,
	FINALIZADO
}
