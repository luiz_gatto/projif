package br.com.yanivilasboas.projif.domain;

public enum tipoMembro {
	DISCENTE,
	DOCENTE,
	SERVIDOR,
	EXTERNO
}
