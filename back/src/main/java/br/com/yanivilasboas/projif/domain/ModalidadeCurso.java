package br.com.yanivilasboas.projif.domain;

public enum ModalidadeCurso {
	SUPERIOR,
	TÉCNICO
}
