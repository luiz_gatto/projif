package br.com.yanivilasboas.projif.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.yanivilasboas.projif.domain.Curso;

import br.com.yanivilasboas.projif.repository.CursoRepository;

@RestController
@RequestMapping("/publico/cursos")
public class 																																																																																																																																																																																			CursoResource {
	
	@Autowired
	private CursoRepository cursoRepository;
	
	@GetMapping
	public List<Curso> listarTodos(){
		return cursoRepository.findAll();
	}
	
	@GetMapping("/{idCurso}")
	public Curso buscarpeloId(@PathVariable Long idCurso) {
		return cursoRepository.findById(idCurso).orElse(null);
	}
	
	@DeleteMapping("/{idCurso}")
	public void remover(@PathVariable Long idCurso) {
		cursoRepository.deleteById(idCurso);
	}
	
	@PostMapping
	public Curso cadastrar(@RequestBody Curso curso) {
		return cursoRepository.save(curso);
	}
	
	@PutMapping
	public Curso atualizar(@RequestBody Curso curso) {
		return cursoRepository.save(curso);
	}
	

}
