package br.com.yanivilasboas.projif.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yanivilasboas.projif.domain.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long>{

}
