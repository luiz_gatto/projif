package br.com.yanivilasboas.projif.domain;

public enum TipoProjeto {
	ENSINO,
	EXTENSÃO,
	PESQUISA,
	TCC
}
