package br.com.yanivilasboas.projif.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.yanivilasboas.projif.repository.EditalRepository;
import br.com.yanivilasboas.projif.domain.Edital;

@RestController
@RequestMapping("/publico/editais")
public class EditalResource {
	
	@Autowired
	private EditalRepository editalRepository;
	
	@GetMapping
	public List<Edital> listarTodos(){
		return editalRepository.findAll();
	}
	
	@GetMapping("/{idEdital}")
	public Edital buscarPeloId(@PathVariable Long idEdital) {
		return editalRepository.findById(idEdital).orElse(null);
	}
	
	@DeleteMapping("/{idEdital}")
	public void remover(@PathVariable Long idEdital) {
		editalRepository.deleteById(idEdital);
	}
	
	@PostMapping
	public Edital cadastrar(@RequestBody Edital edital) {
		return editalRepository.save(edital);
	}
	
	@PutMapping
	public Edital atualizar(@RequestBody Edital edital) {
		return editalRepository.save(edital);
	}
	
}
