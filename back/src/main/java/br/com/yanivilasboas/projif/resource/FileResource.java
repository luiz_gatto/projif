package br.com.yanivilasboas.projif.resource;

import br.com.yanivilasboas.projif.domain.Anexo;
import br.com.yanivilasboas.projif.domain.Projeto;
import br.com.yanivilasboas.projif.repository.AnexoRepository;
import br.com.yanivilasboas.projif.util.FileManager;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/file")
@RequiredArgsConstructor
public class FileResource {

    private final AnexoRepository anexoRepository;

    @PostMapping
    @SneakyThrows
    public ResponseEntity<String> upload(@RequestParam MultipartFile arquivo) {
        FileManager fileManager = FileManager.getInstance();
        return ResponseEntity.ok(fileManager.createFile(arquivo.getBytes(), fileManager.getFileExtention(arquivo.getOriginalFilename())));
    }

//    @GetMapping("/baixar/{idProjeto}")
//    @SneakyThrows
//    public ResponseEntity<Anexo> baixar(@PathVariable Long idProjeto) {
//
//    }

    @PostMapping("/download")
    @SneakyThrows
    public byte[] upload(@RequestBody Anexo arquivo) {

        if(!StringUtils.isEmpty(arquivo.getKey())) {
            FileManager fileManager = FileManager.getInstance();
            return fileManager.getFileBytes(fileManager.getFile(arquivo.getKey(),true));
        }

        return Base64.getDecoder().decode(anexoRepository.findById(arquivo.getId()).orElseThrow().getBase64());
    }

    @PostMapping("/save/{idProjeto}")
    @SneakyThrows
    public ResponseEntity<String> save(@PathVariable Long idProjeto, @RequestBody List<Anexo> anexos) {

        anexos.forEach(a -> {

            try {
                a.setBase64(Base64.getEncoder().encodeToString(FileManager.getInstance().getFileBytes(FileManager.getInstance().getFile(a.getKey(), true))));
            } catch (IOException e) {
                e.printStackTrace();
            }
            a.setProjeto(Projeto.builder().idProjeto(idProjeto).build());
            anexoRepository.save(a);

        });


        return ResponseEntity.ok("Arquivo salvo com sucesso!");

    }

}
