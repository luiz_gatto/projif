package br.com.yanivilasboas.projif.domain;

public enum StatusMembro {
	ATIVO,
	INATIVO
}
