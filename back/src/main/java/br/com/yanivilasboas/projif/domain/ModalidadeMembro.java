package br.com.yanivilasboas.projif.domain;

public enum ModalidadeMembro {
	BOLSISTA,
	VOLUNTÁRIO,
	ORIENTADOR
}
