package br.com.yanivilasboas.projif.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.yanivilasboas.projif.repository.MembroRepository;
import br.com.yanivilasboas.projif.domain.Membro;

@RestController
@RequestMapping("/publico/membros")
public class MembroResource {
	
	@Autowired
	private MembroRepository membroRepository;
	
	@GetMapping
	public List<Membro> listarTodos(){
		return membroRepository.findAll();
	}
	
	@GetMapping("/{idMembro}")
	public Membro buscarPeloId(@PathVariable Long idMembro) {
		return membroRepository.findById(idMembro).orElse(null);
	}
	
	@DeleteMapping("/{idMembro}")
	public void remover(@PathVariable Long idMembro) {
		membroRepository.deleteById(idMembro);
	}
	
	@PostMapping
	public Membro cadastrar(@RequestBody Membro membro) {
		return membroRepository.save(membro);
	}
	
	@PutMapping
	public Membro atualizar(@RequestBody Membro membro) {
		return membroRepository.save(membro);
	}
	
	
}
