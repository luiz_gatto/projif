package br.com.yanivilasboas.projif.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Anexo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String nome;

    @Column
    @JsonIgnore
    private String base64;

    @Transient
    private String key;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "id_projeto")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Projeto projeto;
}
