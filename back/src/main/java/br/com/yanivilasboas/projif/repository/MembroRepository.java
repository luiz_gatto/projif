package br.com.yanivilasboas.projif.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yanivilasboas.projif.domain.Membro;

public interface MembroRepository extends JpaRepository<Membro, Long>{

}
