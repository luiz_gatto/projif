package br.com.yanivilasboas.projif.resource;

import br.com.yanivilasboas.projif.config.Config;
import br.com.yanivilasboas.projif.domain.Usuario;
import br.com.yanivilasboas.projif.payload.AuthResponse;
import br.com.yanivilasboas.projif.payload.LoginRequest;
import br.com.yanivilasboas.projif.payload.RedefinicaoSenhaDTO;
import br.com.yanivilasboas.projif.repository.UsuarioRepository;
import br.com.yanivilasboas.projif.security.TokenProvider;
import br.com.yanivilasboas.projif.services.RecuperacaoSenhaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class Auth {

    private final AuthenticationManager authenticationManager;
    private final TokenProvider tokenProvider;
    private final UsuarioRepository usuarioRepository;
    private final PasswordEncoder passwordEncoder;
    private final RecuperacaoSenhaService recuperacaoSenhaService;

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> authenticateUser(@RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getLogin(),
                        loginRequest.getSenha() + Config.salt
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);

        return ResponseEntity.ok(new AuthResponse(token));

    }

    @PostMapping("/signup")
    public Usuario cadastrar(@RequestBody Usuario usuario) {

        usuario.setSenha(passwordEncoder.encode(usuario.getSenha() + Config.salt));

        return usuarioRepository.save(usuario);

    }

    @PostMapping("/recuperarEmail/aadgdfgdfvtcwtetvdrfgdfysgaet/{email}")
    public ResponseEntity<String> enviarEmail(@PathVariable("email") String email) {
        recuperacaoSenhaService.enviarEmailRecuperacaoDeSenha(usuarioRepository.findByEmail(email).orElseThrow());
        return ResponseEntity.ok("");
    }

    @PostMapping("/validarHash/{hash}")
    public ResponseEntity<Boolean> validarHash(@PathVariable("hash") String hash) {
        return ResponseEntity.ok(recuperacaoSenhaService.validarHashDeRecuperacao(hash));
    }

    @PostMapping("/redefinirSenha/{hash}")
    public ResponseEntity<Boolean> trocarSenha(@PathVariable("hash") String hash,@RequestBody @Validated RedefinicaoSenhaDTO redefinicaoSenhaDTO) {
        return ResponseEntity.ok(recuperacaoSenhaService.redefinirSenha(redefinicaoSenhaDTO,hash));
    }


}
