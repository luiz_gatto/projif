package br.com.yanivilasboas.projif.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import br.com.yanivilasboas.projif.domain.Pessoa;

import br.com.yanivilasboas.projif.repository.PessoaRepository;

@RestController
@RequestMapping("/publico/pessoas")
public class PessoaResource {

	@Autowired
	private PessoaRepository pessoaRepository;

	@GetMapping
	public List<Pessoa> listarTodos(){
		return pessoaRepository.findAll();
	}
	
	@GetMapping("/{idPessoa}")
	public Pessoa buscarPeloId(@PathVariable Long idPessoa) {
		return pessoaRepository.findById(idPessoa).orElse(null);
	}
	
	@DeleteMapping("/{idPessoa}")
	public void remover(@PathVariable Long idPessoa) {
		pessoaRepository.deleteById(idPessoa);
		
		
	}
	
	@PostMapping
	public Pessoa cadastrar(@RequestBody Pessoa pessoa) {
		return pessoaRepository.save(pessoa);
	}
	
	
	@PutMapping
	public Pessoa atualizar(@RequestBody Pessoa pessoa) {
		return pessoaRepository.save(pessoa);
	}


}
