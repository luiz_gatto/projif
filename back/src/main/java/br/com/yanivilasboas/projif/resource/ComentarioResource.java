package br.com.yanivilasboas.projif.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.yanivilasboas.projif.repository.ComentarioRepository;
import br.com.yanivilasboas.projif.domain.Comentario;


@RestController
@RequestMapping("/publico/comentarios")
public class ComentarioResource {
	
	@Autowired
	private ComentarioRepository comentarioRepository;
	
	@GetMapping
	public List<Comentario> listarTodos(){
		return comentarioRepository.findAll();
	}
	
	@GetMapping("/{idComentario}")
	public Comentario buscarPeloId(@PathVariable Long idComentario) {
		return comentarioRepository.findById(idComentario).orElse(null);
	}
	
	@DeleteMapping("/{idComentario}")
	public void remover(@PathVariable Long idComentario) {
		comentarioRepository.deleteById(idComentario);
	}
	
	@PostMapping
	public Comentario cadastrar(@RequestBody Comentario comentario) {
		return comentarioRepository.save(comentario);
	}
	
	@PutMapping
	public Comentario atualizar(@RequestBody Comentario comentario) {
		return comentarioRepository.save(comentario);
	}

}
