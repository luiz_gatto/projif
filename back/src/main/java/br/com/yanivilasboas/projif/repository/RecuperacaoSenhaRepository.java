package br.com.yanivilasboas.projif.repository;

import br.com.yanivilasboas.projif.domain.RecuperacaoSenha;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RecuperacaoSenhaRepository extends JpaRepository<RecuperacaoSenha, Integer> {
    Optional<RecuperacaoSenha> findByUsuarioIdUsuario(Long id);
    Optional<RecuperacaoSenha> findByHash(String hash);
}
