package br.com.yanivilasboas.projif.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class Config {
    public static String secret;
    public static String salt;
    public static String email;
    public static String hashSeguranca;
    public static String expirateToken;

    public static String baseUrl;

    @Autowired
    public Config(Environment env) {
        secret = env.getProperty("app.auth.tokenSecret");
        expirateToken = env.getProperty("app.auth.tokenExpirationMsec");
        salt = env.getProperty("app.auth.salt");
        email = env.getProperty("spring.mail.username");
        baseUrl = env.getProperty("app.baseUrl");
        hashSeguranca = env.getProperty("app.auth.hash");
    }

}
