package br.com.yanivilasboas.projif.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yanivilasboas.projif.domain.AreaConhecimento;

public interface AreaConhecimentoRepository extends JpaRepository<AreaConhecimento, Long> {
	
	
}
