package br.com.yanivilasboas.projif.resource;

import br.com.yanivilasboas.projif.domain.Usuario;
import br.com.yanivilasboas.projif.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/publico/usuarios")
public class UsuarioResource {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@GetMapping
	public List<Usuario> listarTodos(){
		return usuarioRepository.findAll();
	}
	
	@GetMapping("/{idUsuario}")
	public Usuario buscarPeloId(@PathVariable Long idUsuario) {
		return usuarioRepository.findById(idUsuario).orElse(null);
	}
	
	@DeleteMapping("/{idUsuario}")
	public void remover(@PathVariable Long idUsuario) {
		usuarioRepository.deleteById(idUsuario);
	}

}
