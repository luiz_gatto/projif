package br.com.yanivilasboas.projif.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yanivilasboas.projif.domain.Comentario;

public interface ComentarioRepository extends JpaRepository<Comentario, Long> {

}
