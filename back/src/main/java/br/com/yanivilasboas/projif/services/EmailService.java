package br.com.yanivilasboas.projif.services;

import br.com.yanivilasboas.projif.config.Config;
import br.com.yanivilasboas.projif.domain.RecuperacaoSenha;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmailService {

    private final TemplateEngine templateEngine;
    private final JavaMailSender javaMailSender;

    public void enviarEmailRedefinicao(String email, RecuperacaoSenha recuperacao) {

        final Context context = new Context();

        context.setVariable("baseUrl", Config.baseUrl);
        context.setVariable("recuperacao", recuperacao);

        List<String> emailsDestinatarios = Collections.singletonList(email);

        String[] array = new String[emailsDestinatarios.size()];
        array = emailsDestinatarios.toArray(array);

        send(array, "[Não responda] - Redefinição de Senha!", templateEngine.process("email/redefinicaoDeSenha", context));

    }

    private void send(String[] destinatarios, String assunto, String corpo) {

        try {

            MimeMessage mailMessages = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mailMessages, true);
            helper.setFrom(Config.email);
            helper.setTo(destinatarios);
            helper.setSubject(assunto);
            helper.setText(corpo, true);

            javaMailSender.send(mailMessages);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}