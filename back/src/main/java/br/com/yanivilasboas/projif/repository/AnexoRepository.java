package br.com.yanivilasboas.projif.repository;

import br.com.yanivilasboas.projif.domain.Anexo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AnexoRepository extends JpaRepository<Anexo,Integer> {

    @Query(value = "select a from anexo a where id_projeto = idProjeto", nativeQuery = true)
    Integer findByProjeto(Long idProjeto);
}
