package br.com.yanivilasboas.projif.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.yanivilasboas.projif.domain.Projeto;
import br.com.yanivilasboas.projif.repository.ProjetoRepository;

@RestController
@RequestMapping("/projetos")
public class ProjetoResource {
	
	@Autowired
	private ProjetoRepository projetoRepository;
	
	@GetMapping
	public List<Projeto> listarTodos(){
		return projetoRepository.findAll();
	}
	
	@GetMapping("/{idProjeto}")
	public Projeto buscarPeloId(@PathVariable Long idProjeto) {
		return projetoRepository.findById(idProjeto).orElse(null);
	}
	
	@DeleteMapping("/{idProjeto}")
	public void remover(@PathVariable Long idProjeto) {
		projetoRepository.deleteById(idProjeto);
	}
	
	@PostMapping
	public Projeto cadastrar(@RequestBody Projeto projeto) {
		return projetoRepository.save(projeto);
	}
	
	@PutMapping
	public Projeto atualizar(@RequestBody Projeto projeto) {
		return projetoRepository.save(projeto);
	}

}
