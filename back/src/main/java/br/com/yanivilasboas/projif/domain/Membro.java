package br.com.yanivilasboas.projif.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
@Data
public class Membro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMembro;

	private ModalidadeMembro modalidade;

	private Double valorBolsa;

	private tipoMembro tipo;

	private StatusMembro status;

	private LocalDate data_inicio;

	private LocalDate data_fim;

	@ManyToOne
	@JoinColumn(name = "Pessoa_idPessoa", referencedColumnName = "idPessoa")
	private Pessoa pessoa;

	@ManyToOne
	@JoinColumn(name = "Curso_idCurso", referencedColumnName = "idCurso")
	private Curso curso;


	@ManyToOne
	@JoinColumn(name = "Projeto_idProjeto", referencedColumnName = "idProjeto")
	private Projeto projeto;

}
