package br.com.yanivilasboas.projif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjifApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjifApplication.class, args);
	}

}
