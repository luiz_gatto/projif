package br.com.yanivilasboas.projif.services;

import br.com.yanivilasboas.projif.config.Config;
import br.com.yanivilasboas.projif.domain.RecuperacaoSenha;
import br.com.yanivilasboas.projif.domain.Usuario;
import br.com.yanivilasboas.projif.payload.RedefinicaoSenhaDTO;
import br.com.yanivilasboas.projif.repository.RecuperacaoSenhaRepository;
import br.com.yanivilasboas.projif.repository.UsuarioRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.ValidationException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RecuperacaoSenhaService {

    private final RecuperacaoSenhaRepository recuperacaoSenhaRepository;
    private final PasswordEncoder passwordEncoder;
    private final UsuarioRepository userRepository;
    private final EmailService emailService;

    private static final String HASH_FORA_DO_PADRAO = "Hash fora do Padrão";


    private RecuperacaoSenha gerarRecuperacao(Usuario usuario) {

        Optional<RecuperacaoSenha> recuperacaoSenhaOptional = recuperacaoSenhaRepository.findByUsuarioIdUsuario(usuario.getIdUsuario());

        recuperacaoSenhaOptional.ifPresent(recuperacaoSenha ->
                recuperacaoSenhaRepository.deleteById(recuperacaoSenha.getId())
        );

        RecuperacaoSenha recuperacaoSenha = new RecuperacaoSenha();
        recuperacaoSenha.setDataValidade(LocalDate.now().plusDays(1));
        recuperacaoSenha.setHash(UUID.randomUUID().toString());
        recuperacaoSenha.setUsuario(usuario);

        return recuperacaoSenhaRepository.save(recuperacaoSenha);

    }

    @SneakyThrows
    public Boolean validarHashDeRecuperacao(String hash) {

        hash = verificaHashProtecao(hash);

        Optional<RecuperacaoSenha> recuperacaoSenhaOptional = recuperacaoSenhaRepository.findByHash(hash);

        if(recuperacaoSenhaOptional.isPresent()) {
            if (LocalDate.now().isAfter(recuperacaoSenhaOptional.get().getDataValidade())) {
                throw new ValidationException(HASH_FORA_DO_PADRAO);
            }
        } else {
            throw new EntityNotFoundException();
        }

        return true;
    }

    @SneakyThrows
    public Boolean redefinirSenha(RedefinicaoSenhaDTO redefinicaoSenhaDTO, String hash) {

        hash = verificaHashProtecao(hash);

        if(!redefinicaoSenhaDTO.getHash().equals(hash)) {
            throw new ValidationException(HASH_FORA_DO_PADRAO);
        }

        Optional<RecuperacaoSenha> recuperacaoSenhaOptional = recuperacaoSenhaRepository.findByHash(hash);

        if(recuperacaoSenhaOptional.isPresent()) {
            if (LocalDate.now().isAfter(recuperacaoSenhaOptional.get().getDataValidade())) {
                throw new ValidationException(HASH_FORA_DO_PADRAO);
            }
        } else {
            throw new EntityNotFoundException();
        }

        var usuario = recuperacaoSenhaOptional.get().getUsuario();

        usuario.setSenha(passwordEncoder.encode(redefinicaoSenhaDTO.getSenha() + Config.salt));

        userRepository.save(usuario);

        recuperacaoSenhaRepository.deleteById(recuperacaoSenhaOptional.get().getId());

        return true;

    }

    @SneakyThrows
    private String verificaHashProtecao(String hash) {

        if (StringUtils.isEmpty(hash)) {
            throw new ValidationException(HASH_FORA_DO_PADRAO);
        }

        if (hash.contains("--")) {

            if (Config.hashSeguranca.equals(hash.split("--")[1])) {
                return hash.split("--")[0];
            } else {
                throw new ValidationException(HASH_FORA_DO_PADRAO);
            }

        }

        return hash;

    }

    public void enviarEmailRecuperacaoDeSenha(Usuario usuario) {

        emailService.enviarEmailRedefinicao(usuario.getEmail(), gerarRecuperacao(usuario));

    }
}
