package br.com.yanivilasboas.projif.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yanivilasboas.projif.domain.Edital;

public interface EditalRepository extends JpaRepository<Edital, Long>{

}
