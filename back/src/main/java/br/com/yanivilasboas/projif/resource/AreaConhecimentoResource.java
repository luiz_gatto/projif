package br.com.yanivilasboas.projif.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.yanivilasboas.projif.domain.AreaConhecimento;

import br.com.yanivilasboas.projif.repository.AreaConhecimentoRepository;

@RestController
@RequestMapping("/publico/areas")
public class AreaConhecimentoResource {
	
	@Autowired
	private AreaConhecimentoRepository areaConhecimentoRepository;
	
	@GetMapping
	public List<AreaConhecimento> listarTodos(){
		return areaConhecimentoRepository.findAll();
	}
	
	@GetMapping("/{idArea}")
	public AreaConhecimento buscarPeloId(@PathVariable Long idArea) {
		return areaConhecimentoRepository.findById(idArea).orElse(null);
	}
	
	@DeleteMapping("/{idArea}")
	public void remover(@PathVariable Long idArea) {
		areaConhecimentoRepository.deleteById(idArea);
	}
	
	@PostMapping
	public AreaConhecimento cadastrar(@RequestBody AreaConhecimento area) {
		return areaConhecimentoRepository.save(area);
	}
	
	@PutMapping
	public AreaConhecimento atualizar(@RequestBody AreaConhecimento area) {
		return areaConhecimentoRepository.save(area);
	}
}
