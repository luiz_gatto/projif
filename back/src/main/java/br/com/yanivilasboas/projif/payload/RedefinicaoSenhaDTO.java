package br.com.yanivilasboas.projif.payload;

import lombok.Data;

@Data
public class RedefinicaoSenhaDTO {

    private String senha;

    private String hash;

}
