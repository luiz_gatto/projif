package br.com.yanivilasboas.projif.resource;

import br.com.yanivilasboas.projif.repository.ComentarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.yanivilasboas.projif.repository.AnexoRepository;
import br.com.yanivilasboas.projif.domain.Anexo;

public class AnexoResource {

    @Autowired
    private AnexoRepository anexoRepository;

    @DeleteMapping("/{id}")
    public void remover(@PathVariable Integer id) {
        anexoRepository.deleteById(id);
    }

}
