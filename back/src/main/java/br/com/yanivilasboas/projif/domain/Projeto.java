package br.com.yanivilasboas.projif.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Projeto {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long idProjeto;
	
	private String titulo;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate data_inicio;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate data_fim;
	
	private StatusProjeto status;
	
	private TipoProjeto tipo;
	
	private TipoAcessoProjeto acesso;
	
	private String resumo;
	
	private String palavrasChave;

	@OneToMany(mappedBy = "projeto")
	private List<Anexo> anexos;
	


	@ManyToOne
    @JoinColumn(name="edital_idEdital", referencedColumnName = "idEdital")
    private Edital edital;
	
	@ManyToOne
    @JoinColumn(name="areaConhecimento_idArea", referencedColumnName = "idArea")
    private AreaConhecimento areaConhecimento;

	
}
